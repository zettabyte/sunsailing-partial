import json

from django.http import HttpResponse
from django.contrib.auth.decorators import login_required

from .models import SinglePhoto


@login_required
def single_photo_json(request):
    object_id = request.GET.get('object_id')
    content_type_id = request.GET.get('content_type_id')
    photos = SinglePhoto.objects.filter(content_type_id=content_type_id, object_id=object_id)
    data = []
    for photo in photos:
        data.append({'thumb':photo.thumbnail.url, 'image':photo.image.url})
    return HttpResponse(json.dumps(data), content_type='application/json')
