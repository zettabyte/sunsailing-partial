from django.contrib.contenttypes.admin import GenericTabularInline
from django.forms import ModelForm, BooleanField

from utils import image_widgets

from .models import *


class YachtPhotoGalleryInline(GenericTabularInline):
    model = YachtPhotoGallery
    fields = ('image', 'charter_img', 'order_id')
    ordering = ('order_id',)
    extra = 0
    formfield_overrides = {models.ImageField: {'widget': image_widgets.AdminImagePreviewWidget}}


class PhotoWatermarkForm(ModelForm):
    put_watermark = BooleanField(required=False, label='Put watermark?')

    def save(self, commit=True, *args, **kwargs):
        instance = super(PhotoWatermarkForm, self).save(commit=False)
        if commit:
            instance.save(put_watermark=self.cleaned_data['put_watermark'])
        return instance


class SinglePhotoInline(GenericTabularInline):
    fields = ('image', 'put_watermark')
    form = PhotoWatermarkForm
    model = SinglePhoto
    extra = 0
    formfield_overrides = {models.ImageField: {'widget': image_widgets.AdminImagePreviewWidget}}


class GalleryPhotoInline(GenericTabularInline):
    fields = ('image', 'put_watermark', 'alt', 'order_id')
    form = PhotoWatermarkForm
    model = GalleryPhoto
    extra = 0
    formfield_overrides = {models.ImageField: {'widget': image_widgets.AdminImagePreviewWidget}}
