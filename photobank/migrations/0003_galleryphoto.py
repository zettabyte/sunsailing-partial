# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import photobank.models


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        ('photobank', '0002_yachtphotogallery_charter_img'),
    ]

    operations = [
        migrations.CreateModel(
            name='GalleryPhoto',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('image', models.ImageField(upload_to=photobank.models.GalleryPhoto.image_path)),
                ('thumbnail', models.ImageField(upload_to=photobank.models.GalleryPhoto.thumbnail_path)),
                ('object_id', models.PositiveIntegerField()),
                ('content_type', models.ForeignKey(to='contenttypes.ContentType')),
            ],
        ),
    ]
