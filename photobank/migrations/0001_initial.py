# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import photobank.models


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
    ]

    operations = [
        migrations.CreateModel(
            name='SinglePhoto',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('image', models.ImageField(upload_to=photobank.models.SinglePhoto.image_path)),
                ('thumbnail', models.ImageField(upload_to=photobank.models.SinglePhoto.thumbnail_path)),
                ('watermark', models.BooleanField(default=False)),
                ('object_id', models.PositiveIntegerField()),
                ('content_type', models.ForeignKey(to='contenttypes.ContentType')),
            ],
        ),
        migrations.CreateModel(
            name='YachtPhotoGallery',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('image', models.ImageField(upload_to=photobank.models.YachtPhotoGallery.image_path)),
                ('thumbnail', models.ImageField(upload_to=photobank.models.YachtPhotoGallery.thumbnail_path)),
                ('order_id', models.PositiveIntegerField(null=True, blank=True)),
                ('object_id', models.PositiveIntegerField()),
                ('content_type', models.ForeignKey(to='contenttypes.ContentType')),
            ],
            options={
                'ordering': ('order_id',),
            },
        ),
        migrations.CreateModel(
            name='SinglePhotoWatermark',
            fields=[
            ],
            options={
                'proxy': True,
            },
            bases=('photobank.singlephoto',),
        ),
    ]
