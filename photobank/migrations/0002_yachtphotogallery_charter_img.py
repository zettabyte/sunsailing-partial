# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photobank', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='yachtphotogallery',
            name='charter_img',
            field=models.BooleanField(default=False),
        ),
    ]
