# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photobank', '0004_galleryphoto_alt'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='singlephoto',
            name='watermark',
        ),
    ]
