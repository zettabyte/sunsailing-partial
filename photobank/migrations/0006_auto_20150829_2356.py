# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photobank', '0005_remove_singlephoto_watermark'),
    ]

    operations = [
        migrations.DeleteModel(
            name='SinglePhotoWatermark',
        ),
        migrations.AlterModelOptions(
            name='galleryphoto',
            options={'ordering': ('order_id',)},
        ),
        migrations.AddField(
            model_name='galleryphoto',
            name='order_id',
            field=models.PositiveIntegerField(blank=True, null=True),
        ),
    ]
