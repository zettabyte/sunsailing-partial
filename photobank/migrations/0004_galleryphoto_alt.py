# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photobank', '0003_galleryphoto'),
    ]

    operations = [
        migrations.AddField(
            model_name='galleryphoto',
            name='alt',
            field=models.CharField(null=True, blank=True, max_length=255),
        ),
    ]
