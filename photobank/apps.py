from django.apps import AppConfig



class PhotobankConfig(AppConfig):
    name = 'photobank'
    verbose_name = 'Photo bank'

    def ready(self):
        import photobank.signals