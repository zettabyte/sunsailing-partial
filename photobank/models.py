import os

from django.db import models
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType

from utils import img_utils


class AbstractPhoto(models.Model):
    TMB_RESIZE = {'galleryphoto': 360,
                  'singlephoto': 125}

    def image_path(instance, filename):
        return os.path.join(instance.content_type.app_label, instance.content_type.model, instance._meta.model_name, str(instance.object_id), filename)

    def thumbnail_path(instance, filename):
        return os.path.join(instance.content_type.app_label, instance.content_type.model, instance._meta.model_name, str(instance.object_id), 'thumbnails', filename)

    image = models.ImageField(upload_to=image_path)
    thumbnail = models.ImageField(upload_to=thumbnail_path)

    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey()


    class Meta:
        abstract = True


class AbstractPhotoWatermark(AbstractPhoto):
    def save(self, *args, **kwargs):
        if kwargs.pop('put_watermark'):
            img_file = self.image.file
            self.image = img_utils.put_watermark(img_file)
            img_file.close()

        if not os.path.isfile(self.image.path):
            img_file = self.image.file
            self.thumbnail = img_utils.resize_ratio(self.TMB_RESIZE[self._meta.model_name], img_file)

        super(AbstractPhotoWatermark, self).save(*args, **kwargs)

    class Meta:
        abstract = True


class CharterImageQuerySet(models.QuerySet):
    def charter_images(self):
        return self.filter(charter_img=True)


class YachtPhotoGallery(AbstractPhoto):
    TMB_RESIZE = 230

    charter_img = models.BooleanField(default=False)
    order_id = models.PositiveIntegerField(blank=True, null=True)

    objects = CharterImageQuerySet.as_manager()

    def save(self, *args, **kwargs):
        if not os.path.isfile(self.image.path):
            img_file = self.image.file
            self.image = img_utils.put_watermark(img_file)
            self.thumbnail = img_utils.resize_ratio(self.TMB_RESIZE, img_file)
        super(YachtPhotoGallery, self).save(*args, **kwargs)

    class Meta:
        ordering = ('order_id',)


class SinglePhoto(AbstractPhotoWatermark):
    pass


class GalleryPhoto(AbstractPhotoWatermark):
    alt = models.CharField(max_length=255, null=True, blank=True)
    order_id = models.PositiveIntegerField(blank=True, null=True)

    class Meta:
        ordering = ('order_id',)