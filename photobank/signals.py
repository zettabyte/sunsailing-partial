from django.db.models.signals import pre_save, pre_delete
from django.dispatch import receiver

from utils import signal_utils

from .models import YachtPhotoGallery, SinglePhoto, GalleryPhoto


@receiver(pre_save, sender=YachtPhotoGallery)
@receiver(pre_save, sender=SinglePhoto)
@receiver(pre_save, sender=GalleryPhoto)
def replace_item(instance, sender, **kwargs):
    signal_utils.del_file_on_replace_item(instance, sender)


@receiver(pre_delete, sender=YachtPhotoGallery)
@receiver(pre_delete, sender=SinglePhoto)
@receiver(pre_delete, sender=GalleryPhoto)
def del_item(instance, **kwargs):
    signal_utils.del_file_on_del_item(instance)