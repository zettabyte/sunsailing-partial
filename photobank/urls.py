from django.conf.urls import url

from .views import *


urlpatterns = [
    url(r'^single-photo/json/$', single_photo_json, name='single_photo_json'),
    ]
