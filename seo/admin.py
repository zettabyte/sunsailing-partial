from django.contrib import admin
from django import forms

from easy_select2 import apply_select2

from .models import *


class MetaDataForm(forms.ModelForm):
    class Meta:
        model = MetaData
        fields = ('category', 'url', 'title', 'description', 'keywords')
        widgets = {'title': forms.TextInput(attrs={'size': '55'}),
                   'description': forms.TextInput(attrs={'size': '115'}),
                   'category': apply_select2(forms.Select),
                   'url': forms.TextInput(attrs={'size': '160'}),
        }


class MetaDataAdmin(admin.ModelAdmin):
    form = MetaDataForm
    search_fields = ['title', 'url']
    list_display = ('title', 'category', 'url')
    list_filter = ('category',)


admin.site.register(Category)
admin.site.register(MetaData, MetaDataAdmin)