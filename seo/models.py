from django.db import models


class Category(models.Model):
    title = models.CharField(max_length=255)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ('title',)


class MetaData(models.Model):
    title = models.CharField(max_length=55, null=True, blank=True)
    category = models.ForeignKey(Category, null=True, blank=True)
    description = models.CharField(max_length=115, null=True, blank=True)
    keywords = models.CharField(max_length=255, null=True, blank=True)
    url = models.CharField(max_length=255, unique=True, null=True)

    def __str__(self):
        return self.title