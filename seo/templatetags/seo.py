from django import template
from django.utils import html

from seo.models import MetaData


register = template.Library()


@register.inclusion_tag('seo/meta-data.html')
def meta_data(url, page):
    try:
        res = MetaData.objects.get(url=url)
        if page and page > 1:
            res.title = res.title + ' | Страница ' + str(page)
            res.description = res.description + ' Страница ' + str(page)
    except:
        res = None
    return {'meta_item': res}


@register.simple_tag(takes_context=True)
def path_to_page(context, page_num):
    qd = context.request.GET.copy()
    if 'page' in qd:
        qd.pop('page')
    if page_num != 1:
        qd.appendlist('page', page_num)
    res = context.request.path + '?' + qd.urlencode() if qd.urlencode() else context.request.path
    return html.escape(res)


@register.simple_tag
def fb_id():
    return 378082878949655


@register.simple_tag
def site_name():
    return 'sunSailing'