# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('seo', '0003_auto_20150711_1431'),
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('title', models.CharField(max_length=255)),
            ],
            options={
                'ordering': ('title',),
            },
        ),
        migrations.AlterUniqueTogether(
            name='metadata',
            unique_together=set([]),
        ),
        migrations.RemoveField(
            model_name='metadata',
            name='content_type',
        ),
        migrations.RemoveField(
            model_name='metadata',
            name='object_id',
        ),
        migrations.AddField(
            model_name='metadata',
            name='category',
            field=models.ForeignKey(blank=True, to='seo.Category', null=True),
        ),
    ]
