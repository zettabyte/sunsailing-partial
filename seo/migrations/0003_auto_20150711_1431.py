# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('seo', '0002_metadata_url'),
    ]

    operations = [
        migrations.AlterField(
            model_name='metadata',
            name='url',
            field=models.CharField(max_length=255, unique=True, null=True),
        ),
    ]
