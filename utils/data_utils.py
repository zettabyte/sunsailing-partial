def cabin_flat_list(values_list):
    cabin_set = set()
    for i in values_list:
        cabin_set = cabin_set | set(i.split(','))
    return list(sorted(map(int, cabin_set)))
