def del_file_on_replace_item(instance, sender):
    for field in instance._meta.get_fields():
        try:
            if field.get_internal_type() == 'FileField':
                exist_item = getattr(sender.objects.get(id=instance.id), field.__dict__['name'])
                new_item = getattr(instance, field.__dict__['name'])
                if exist_item != new_item:
                    exist_item.delete(save=False)
        except:
            pass

def del_file_on_del_item(instance):
    for field in instance._meta.get_fields():
        try:
            if field.get_internal_type() == 'FileField':
                item = getattr(instance, field.__dict__['name'])
                item.delete(save=False)
        except:
            pass