from django.core.mail import send_mail

def form_email_notification(subj, body):
    send_mail(subj, body, 'noreply@sunsailing.com.ua', ['sunsailingua@gmail.com'], fail_silently=True)