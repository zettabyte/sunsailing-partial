import io
from PIL import Image

from django.conf import settings
from django.core.files.uploadedfile import InMemoryUploadedFile


def resize_ratio(len, image):
    img = Image.open(image)
    img.thumbnail((len, len))
    io_img = io.BytesIO()
    img.save(io_img, format='JPEG')
    return InMemoryUploadedFile(io_img, None, image.name, 'image/jpeg', io_img.getbuffer().nbytes, None)


def put_watermark(image):
    watermark = Image.open(settings.WATERMARK)
    img = Image.open(image)
    img.paste(watermark, (img.size[0] - watermark.size[0] - 5, img.size[1] - watermark.size[1] - 5), watermark)
    io_img = io.BytesIO()
    img.save(io_img, format='JPEG')
    return InMemoryUploadedFile(io_img, None, image.name, 'image/jpeg', io_img.getbuffer().nbytes, None)