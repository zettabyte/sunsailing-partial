from django import forms
from django.utils.safestring import mark_safe
from django.core.urlresolvers import reverse



class AdminImagePreviewWidget(forms.FileInput):
    def __init__(self, attrs={}):
        super(AdminImagePreviewWidget, self).__init__(attrs)

    def render(self, name, value, attrs=None):
        output = []
        if value and hasattr(value, "url"):
            output.append(('<a target="_blank" href="%s">'
                           '<img src="%s" style="height: 120px;" /></a> '
                           % (value.url, value.url)))
        output.append(super(AdminImagePreviewWidget, self).render(name, value, attrs))
        return mark_safe(''.join(output))


def single_photo_json_url(object_id, content_type_id):
    return reverse('photobank:single_photo_json')+'?object_id='+str(object_id)+'&content_type_id='+str(content_type_id)