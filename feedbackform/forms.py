from django import forms
from django.template.defaultfilters import date as datefilter

from school.models import TheoryStart



class BasicRequestForm(forms.Form):
    name = forms.CharField(label='Имя*', max_length=255, widget=forms.TextInput(attrs={'class': 'form-control'}))
    phone = forms.CharField(label='Номер телефона*', max_length=255, widget=forms.TextInput(attrs={'class': 'form-control'}))
    email = forms.EmailField(label='Email адрес*', max_length=255, widget=forms.EmailInput(attrs={'class': 'form-control'}))
    message = forms.CharField(label='Текст сообщения', max_length=255, required=False, widget=forms.Textarea(attrs={'class': 'form-control', 'rows':'7'}))


class ContactUsForm(forms.Form):
    name = forms.CharField(label='Имя', max_length=255, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Имя'}))
    email = forms.EmailField(label='Email адрес', max_length=255, widget=forms.EmailInput(attrs={'class': 'form-control', 'placeholder': 'Email адрес'}))
    message = forms.CharField(label='Текст сообщения', max_length=255, widget=forms.Textarea(attrs={'class': 'form-control', 'rows':'6', 'placeholder': 'Текст сообщения'}))

    def __init__(self, *args, **kwargs):
        super(ContactUsForm, self).__init__(*args, **kwargs)
        self.auto_id = 'contactus_id_%s'


class FaqForm(forms.Form):
    name = forms.CharField(label='Имя', max_length=255, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Имя'}))
    email = forms.EmailField(label='Email адрес', max_length=255, widget=forms.EmailInput(attrs={'class': 'form-control', 'placeholder': 'Email адрес'}))
    message = forms.CharField(label='Текст вопроса', max_length=255, widget=forms.Textarea(attrs={'class': 'form-control', 'rows':'6', 'placeholder': 'Текст вопроса'}))


class YachtCatalogueForm(BasicRequestForm):
    SKIPPER_REQUIRED = (('Нужен шкипер', 'Нужен шкипер'), ('Без шкипера', 'Без шкипера'))
    period = forms.CharField(label='Желаемые даты круиза*', max_length=255, widget=forms.TextInput(attrs={'class': 'form-control'}))
    country = forms.CharField(label='Страна для круиза*', max_length=255, widget=forms.TextInput(attrs={'class': 'form-control'}))
    skipper = forms.ChoiceField(label='Услуги шкипера', widget=forms.RadioSelect, choices=SKIPPER_REQUIRED, initial=SKIPPER_REQUIRED[0][0])


class CalculateYachtCharterFrom(YachtCatalogueForm):
    YACHT_TYPE = (('Парусная яхта', 'Парусная яхта'),
                  ('Катамаран', 'Катамаран'),
                  ('Моторная яхта', 'Моторная яхта'),
                  ('Гулет', 'Гулет'),
                  ('Люкс яхта', 'Люкс яхта'))
    people = forms.CharField(label='Количество человек на борту*', max_length=255, widget=forms.TextInput(attrs={'class': 'form-control'}))
    yacht_type = forms.ChoiceField(label='Предпочитаемый тип яхты', widget=forms.RadioSelect, choices=YACHT_TYPE, initial=YACHT_TYPE[0][0])


class CrewedYachtCharterForm(BasicRequestForm):
    duration = forms.CharField(label='Длительность аренды (дней)*', widget=forms.TextInput(attrs={'class': 'form-control'}), max_length=255)


class CabinCharterForm(BasicRequestForm):
    people = forms.CharField(label='Количество людей*', max_length=255, widget=forms.TextInput(attrs={'class': 'form-control'}))


class SchoolTheoryForm(BasicRequestForm):
    def __init__(self, *args, **kwargs):
        super(SchoolTheoryForm, self).__init__(*args, **kwargs)
        start_dates =[(datefilter(i.start_date, 'j E Y'), datefilter(i.start_date, 'j E Y')) for i in TheoryStart.objects.all()]
        start_dates.append(('Дистанционный курс', 'Дистанционный курс'))
        self.fields['start_dates'] = forms.ChoiceField(label='Начало занятий', choices=start_dates, widget=forms.RadioSelect)