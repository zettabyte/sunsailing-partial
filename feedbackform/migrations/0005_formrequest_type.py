# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('feedbackform', '0004_remove_formrequest_type'),
    ]

    operations = [
        migrations.AddField(
            model_name='formrequest',
            name='type',
            field=models.CharField(null=True, max_length=255, choices=[('Расчитать тур', 'Расчитать тур'), ('Каталог яхт', 'Каталог яхт'), ('Аренда яхт', 'Аренда яхт')]),
        ),
    ]
