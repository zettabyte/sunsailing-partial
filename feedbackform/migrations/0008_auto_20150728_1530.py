# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('feedbackform', '0007_auto_20150714_1547'),
    ]

    operations = [
        migrations.AlterField(
            model_name='feedbacktyperequest',
            name='type',
            field=models.CharField(unique=True, max_length=255, choices=[('Calculate yacht charter', 'Calculate yacht charter'), ('Bareboat yacht charter', 'Bareboat yacht charter'), ('Crewed yacht charter', 'Crewed yacht charter'), ('Cabin charter', 'Cabin charter'), ('School theory', 'School theory'), ('School cruise', 'School cruise'), ('Yacht catalogue', 'Yacht catalogue'), ('Contact us', 'Contact us'), ('Ask question', 'Ask question')]),
        ),
        migrations.AlterField(
            model_name='formrequest',
            name='type',
            field=models.CharField(null=True, max_length=255, choices=[('Calculate yacht charter', 'Calculate yacht charter'), ('Bareboat yacht charter', 'Bareboat yacht charter'), ('Crewed yacht charter', 'Crewed yacht charter'), ('Cabin charter', 'Cabin charter'), ('School theory', 'School theory'), ('School cruise', 'School cruise'), ('Yacht catalogue', 'Yacht catalogue'), ('Contact us', 'Contact us'), ('Ask question', 'Ask question')]),
        ),
    ]
