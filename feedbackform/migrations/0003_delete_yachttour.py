# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('feedbackform', '0002_feedbacktyperequest_formrequest'),
    ]

    operations = [
        migrations.DeleteModel(
            name='YachtTour',
        ),
    ]
