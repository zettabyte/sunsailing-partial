# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('feedbackform', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='FeedbackTypeRequest',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('type', models.CharField(choices=[('tour calculation', 'Расчитать тур'), ('yacht catalog', 'Каталог яхт'), ('yacht catalog', 'Аренда яхт')], max_length=255, unique=True)),
                ('text_top', models.TextField(blank=True, null=True)),
                ('text_bottom', models.TextField(blank=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='FormRequest',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=255)),
                ('phone', models.CharField(max_length=255)),
                ('email', models.EmailField(max_length=254)),
                ('order', models.TextField()),
                ('submission_date', models.DateTimeField(auto_now_add=True)),
                ('type', models.ForeignKey(to='feedbackform.FeedbackTypeRequest')),
            ],
            options={
                'ordering': ('-submission_date',),
            },
        ),
    ]
