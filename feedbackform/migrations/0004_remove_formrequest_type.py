# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('feedbackform', '0003_delete_yachttour'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='formrequest',
            name='type',
        ),
    ]
