# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='YachtTour',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('phone', models.CharField(max_length=255)),
                ('email', models.EmailField(max_length=254)),
                ('message', models.TextField()),
                ('dates', models.CharField(max_length=255)),
                ('country', models.CharField(max_length=255)),
                ('person_num', models.CharField(max_length=255)),
                ('yacht_type', models.CharField(choices=[('sailingyacht', 'Парусная яхта'), ('catamaran', 'Катамаран'), ('motoryacht', 'Моторная яхта'), ('gulet', 'Гулет'), ('luxyacht', 'Люкс яхта')], max_length=255)),
                ('need_skipper', models.CharField(choices=[('yes', 'Да'), ('no', 'Нет')], max_length=255, default='yes')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
