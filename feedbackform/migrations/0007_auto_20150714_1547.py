# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('feedbackform', '0006_auto_20150714_1538'),
    ]

    operations = [
        migrations.AlterField(
            model_name='feedbacktyperequest',
            name='type',
            field=models.CharField(unique=True, choices=[('Расчитать тур', 'Расчитать тур'), ('Каталог яхт', 'Каталог яхт'), ('Аренда яхт', 'Аренда яхт')], max_length=255),
        ),
        migrations.AlterField(
            model_name='formrequest',
            name='type',
            field=models.CharField(null=True, choices=[('Расчитать тур', 'Расчитать тур'), ('Каталог яхт', 'Каталог яхт'), ('Аренда яхт', 'Аренда яхт')], max_length=255),
        ),
    ]
