# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('feedbackform', '0005_formrequest_type'),
    ]

    operations = [
        migrations.AlterField(
            model_name='formrequest',
            name='type',
            field=models.CharField(null=True, max_length=255, choices=[('tour calculation', 'Расчитать тур'), ('yacht catalog', 'Каталог яхт'), ('yacht catalog', 'Аренда яхт')]),
        ),
    ]
