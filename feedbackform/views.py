import json

from django.shortcuts import render, get_object_or_404
from django.template.defaultfilters import date as datefilter
from django.http import HttpResponse, HttpResponseServerError, Http404
from django.core.urlresolvers import reverse
from django.views.generic.edit import FormView
from django.views.generic.base import TemplateView

from .forms import CalculateYachtCharterFrom, BasicRequestForm, CrewedYachtCharterForm, CabinCharterForm, SchoolTheoryForm, \
    YachtCatalogueForm, ContactUsForm
from feedbackform.models import FormRequest, FEEDBACK_REQUEST, FeedbackTypeRequest
from yachtcharter.models import YachtCharterBareboatPriceLine, YachtCharterCrewedPriceLine, Yacht
from cabincharter.models import CharterPriceLine
from school.models import Price as SchoolCruisePrice
from utils.email_utils import form_email_notification


class ThankYouView(TemplateView):
    template_name = 'feedbackform/thanks.html'

    def get_context_data(self, **kwargs):
        if self.request.session.get('order_id', 0) == int(self.kwargs['order_id']):
            context = super(ThankYouView, self).get_context_data(**kwargs)
            self.request.session.set_expiry(0)
            return context
        else:
            raise Http404()


class FormSuccessUrlMixin:
    def get_success_url(self):
        return reverse('feedbackform:thankyou', kwargs={'order_id': self.request.session['order_id']})


class TourCalculationView(FormSuccessUrlMixin, FormView):
    template_name = 'feedbackform/calculate_yacht_charter.html'
    form_class = CalculateYachtCharterFrom

    def form_valid(self, form):
        order = 'Имя        —    {0}\n' \
                'Телефон    —    {1}\n' \
                'Email      —    {2}\n' \
                'Период     —    {3}\n' \
                'Страна     —    {4}\n' \
                'Человек    —    {5}\n' \
                'Яхта       —    {6}\n' \
                'Шкипер     —    {7}\n\n' \
                'Текст сообщения:\n{8}'.format(form.cleaned_data['name'],
                                               form.cleaned_data['phone'],
                                               form.cleaned_data['email'],
                                               form.cleaned_data['period'],
                                               form.cleaned_data['country'],
                                               form.cleaned_data['people'],
                                               form.cleaned_data['yacht_type'],
                                               form.cleaned_data['skipper'],
                                               form.cleaned_data['message'])
        order_item = FormRequest(type=FEEDBACK_REQUEST[0][0],
                    name=form.cleaned_data['name'],
                    phone=form.cleaned_data['phone'],
                    email=form.cleaned_data['email'],
                    order=order)
        order_item.save()
        form_email_notification(FEEDBACK_REQUEST[0][0], order)
        self.request.session['order_id'] = order_item.id
        return super(TourCalculationView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(TourCalculationView, self).get_context_data(**kwargs)
        context['form_extras'] = FeedbackTypeRequest.objects.filter(type=FEEDBACK_REQUEST[0][0])
        return context


class BareboatYachtCharterView(FormSuccessUrlMixin, FormView):
    template_name = 'feedbackform/bareboat_yacht_charter.html'
    form_class = BasicRequestForm

    def form_valid(self, form):
        obj = get_object_or_404(YachtCharterBareboatPriceLine, id=self.kwargs['price_id'])
        order = 'Имя          —    {0}\n' \
                'Телефон      —    {1}\n' \
                'Email        —    {2}\n' \
                'Яхта         —    {3}\n' \
                'Страна       —    {4}\n' \
                'Порт         —    {5}\n' \
                'Период       —    {6}\n' \
                'Стоимость    —    {7} €\n\n' \
                'Текст сообщения:\n{8}'.format(form.cleaned_data['name'],
                                               form.cleaned_data['phone'],
                                               form.cleaned_data['email'],
                                               obj.price_list.yacht_charter.yacht.title,
                                               obj.price_list.yacht_charter.country.title,
                                               obj.price_list.yacht_charter.start_port,
                                               datefilter(obj.date_from, 'j E Y') + ' — ' + datefilter(obj.date_to, 'j E Y'),
                                               obj.final_price,
                                               form.cleaned_data['message'])
        order_item = FormRequest(type=FEEDBACK_REQUEST[1][0],
                    name=form.cleaned_data['name'],
                    phone=form.cleaned_data['phone'],
                    email=form.cleaned_data['email'],
                    order=order)
        order_item.save()
        form_email_notification(FEEDBACK_REQUEST[1][0], order)
        self.request.session['order_id'] = order_item.id
        return super(BareboatYachtCharterView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(BareboatYachtCharterView, self).get_context_data(**kwargs)
        context['form_extras'] = FeedbackTypeRequest.objects.filter(type=FEEDBACK_REQUEST[1][0])
        context['price_id'] = self.kwargs['price_id']
        context['obj'] = get_object_or_404(YachtCharterBareboatPriceLine, id=self.kwargs['price_id'])
        return context


class CrewedYachtCharterView(FormSuccessUrlMixin, FormView):
    template_name = 'feedbackform/crewed_yacht_charter.html'
    form_class = CrewedYachtCharterForm

    def form_valid(self, form):
        obj = get_object_or_404(YachtCharterCrewedPriceLine, id=self.kwargs['price_id'])
        order = 'Имя                    —    {0}\n' \
                'Телефон                —    {1}\n' \
                'Email                  —    {2}\n' \
                'Яхта                   —    {3}\n' \
                'Страна                 —    {4}\n' \
                'Порт                   —    {5}\n' \
                'Период                 —    {6}\n' \
                'Длительность (дней)    —    {7}\n' \
                'Стоимость(неделя)      —    {8} €\n' \
                'Стоимость(день)        —    {9} €\n\n' \
                'Текст сообщения:\n{10}'.format(form.cleaned_data['name'],
                                                form.cleaned_data['phone'],
                                                form.cleaned_data['email'],
                                                obj.price_list.yacht_charter.yacht.title,
                                                obj.price_list.yacht_charter.country.title,
                                                obj.price_list.yacht_charter.start_port,
                                                datefilter(obj.date_from, 'j E Y') + ' — ' + datefilter(obj.date_to, 'j E Y'),
                                                form.cleaned_data['duration'],
                                                obj.week_price,
                                                obj.day_price,
                                                form.cleaned_data['message'])
        order_item = FormRequest(type=FEEDBACK_REQUEST[2][0],
                    name=form.cleaned_data['name'],
                    phone=form.cleaned_data['phone'],
                    email=form.cleaned_data['email'],
                    order=order)
        order_item.save()
        form_email_notification(FEEDBACK_REQUEST[2][0], order)
        self.request.session['order_id'] = order_item.id
        return super(CrewedYachtCharterView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(CrewedYachtCharterView, self).get_context_data(**kwargs)
        context['form_extras'] = FeedbackTypeRequest.objects.filter(type=FEEDBACK_REQUEST[2][0])
        context['price_id'] = self.kwargs['price_id']
        context['obj'] = get_object_or_404(YachtCharterCrewedPriceLine, id=self.kwargs['price_id'])
        return context


class CabinCharterView(FormSuccessUrlMixin, FormView):
    template_name = 'feedbackform/cabin_charter.html'
    form_class = CabinCharterForm

    def form_valid(self, form):
        obj = get_object_or_404(CharterPriceLine, id=self.kwargs['price_id'])
        order = 'Имя               —    {0}\n' \
                'Телефон           —    {1}\n' \
                'Email             —    {2}\n' \
                'Название          —    {3}\n' \
                'Период            —    {4}\n' \
                'Человек           —    {5}\n' \
                '1 чел. в каюте    —    {6}\n' \
                '2 чел. в каюте    —    {7} €\n' \
                '3 чел. в каюте    —    {8} €\n\n' \
                'Текст сообщения:\n{9}'.format(form.cleaned_data['name'],
                                               form.cleaned_data['phone'],
                                               form.cleaned_data['email'],
                                               obj.charter_price.offer.title,
                                               datefilter(obj.date_from, 'j E Y') + ' — ' + datefilter(obj.date_to, 'j E Y'),
                                               form.cleaned_data['people'],
                                               obj.one_person_price,
                                               obj.two_person_price,
                                               obj.three_person_price,
                                               form.cleaned_data['message'])
        order_item = FormRequest(type=FEEDBACK_REQUEST[3][0],
                    name=form.cleaned_data['name'],
                    phone=form.cleaned_data['phone'],
                    email=form.cleaned_data['email'],
                    order=order)
        order_item.save()
        form_email_notification(FEEDBACK_REQUEST[3][0], order)
        self.request.session['order_id'] = order_item.id
        return super(CabinCharterView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(CabinCharterView, self).get_context_data(**kwargs)
        context['price_id'] = self.kwargs['price_id']
        context['obj'] = get_object_or_404(CharterPriceLine, id=self.kwargs['price_id'])
        context['form_extras'] = FeedbackTypeRequest.objects.filter(type=FEEDBACK_REQUEST[3][0])
        return context


class SchoolTheoryView(FormSuccessUrlMixin, FormView):
    template_name = 'feedbackform/school_theory.html'
    form_class = SchoolTheoryForm

    def form_valid(self, form):
        order = 'Имя               —    {0}\n' \
                'Телефон           —    {1}\n' \
                'Email             —    {2}\n' \
                'Начало занятий    —    {3}\n' \
                'Текст сообщения:\n{4}'.format(form.cleaned_data['name'],
                                               form.cleaned_data['phone'],
                                               form.cleaned_data['email'],
                                               form.cleaned_data['start_dates'],
                                               form.cleaned_data['message'])
        order_item = FormRequest(type=FEEDBACK_REQUEST[4][0],
                    name=form.cleaned_data['name'],
                    phone=form.cleaned_data['phone'],
                    email=form.cleaned_data['email'],
                    order=order)
        order_item.save()
        form_email_notification(FEEDBACK_REQUEST[4][0], order)
        self.request.session['order_id'] = order_item.id
        return super(SchoolTheoryView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(SchoolTheoryView, self).get_context_data(**kwargs)
        context['form_extras'] = FeedbackTypeRequest.objects.filter(type=FEEDBACK_REQUEST[4][0])
        return context


class SchoolCruiseView(FormSuccessUrlMixin, FormView):
    template_name = 'feedbackform/school_cruise.html'
    form_class = BasicRequestForm

    def form_valid(self, form):
        obj = get_object_or_404(SchoolCruisePrice, id=self.kwargs['price_id'])
        order = 'Имя          —    {0}\n' \
                'Телефон      —    {1}\n' \
                'Email        —    {2}\n' \
                'Название     —    {3}\n' \
                'Период       —    {4}\n' \
                'Стоимость    —    {5} €\n\n' \
                'Текст сообщения:\n{6}'.format(form.cleaned_data['name'],
                                               form.cleaned_data['phone'],
                                               form.cleaned_data['email'],
                                               obj.cruise.title,
                                               datefilter(obj.date_from, 'j E Y') + ' — ' + datefilter(obj.date_to, 'j E Y'),
                                               obj.price,
                                               form.cleaned_data['message'])
        order_item = FormRequest(type=FEEDBACK_REQUEST[5][0],
                    name=form.cleaned_data['name'],
                    phone=form.cleaned_data['phone'],
                    email=form.cleaned_data['email'],
                    order=order)
        order_item.save()
        form_email_notification(FEEDBACK_REQUEST[5][0], order)
        self.request.session['order_id'] = order_item.id
        return super(SchoolCruiseView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(SchoolCruiseView, self).get_context_data(**kwargs)
        context['price_id'] = self.kwargs['price_id']
        context['obj'] = get_object_or_404(SchoolCruisePrice, id=self.kwargs['price_id'])
        context['form_extras'] = FeedbackTypeRequest.objects.filter(type=FEEDBACK_REQUEST[5][0])
        return context


class YachtCatalogueView(FormSuccessUrlMixin, FormView):
    template_name = 'feedbackform/yacht_catalogue.html'
    form_class = YachtCatalogueForm

    def form_valid(self, form):
        obj = get_object_or_404(Yacht, id=self.kwargs['yacht_id'])
        order = 'Имя        —    {0}\n' \
                'Телефон    —    {1}\n' \
                'Email      —    {2}\n' \
                'Яхта       —    {3}\n' \
                'Каюты      —    {4}\n' \
                'Период     —    {5}\n' \
                'Страна     —    {6}\n' \
                'Шкипер     —    {7}\n\n' \
                'Текст сообщения:\n{8}'.format(form.cleaned_data['name'],
                                               form.cleaned_data['phone'],
                                               form.cleaned_data['email'],
                                               obj.title,
                                               obj.cabin_display,
                                               form.cleaned_data['period'],
                                               form.cleaned_data['country'],
                                               form.cleaned_data['skipper'],
                                               form.cleaned_data['message'])
        order_item = FormRequest(type=FEEDBACK_REQUEST[6][0],
                    name=form.cleaned_data['name'],
                    phone=form.cleaned_data['phone'],
                    email=form.cleaned_data['email'],
                    order=order)
        order_item.save()
        form_email_notification(FEEDBACK_REQUEST[6][0], order)
        self.request.session['order_id'] = order_item.id
        return super(YachtCatalogueView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(YachtCatalogueView, self).get_context_data(**kwargs)
        context['yacht_id'] = self.kwargs['yacht_id']
        context['obj'] = get_object_or_404(Yacht, id=self.kwargs['yacht_id'])
        context['form_extras'] = FeedbackTypeRequest.objects.filter(type=FEEDBACK_REQUEST[6][0])

        return context


class ContactUsView(FormView):
    form_class = ContactUsForm

    def form_valid(self, form):
        if self.request.is_ajax():
            order = 'Имя      -    {0}\n' \
                    'Email    -    {1}\n' \
                    'Текст сообщения:\n{2}'.format(form.cleaned_data['name'],
                                                   form.cleaned_data['email'],
                                                   form.cleaned_data['message'])
            FormRequest(type=FEEDBACK_REQUEST[7][0],
                        name=form.cleaned_data['name'],
                        email=form.cleaned_data['email'],
                        order=order).save()
            form_email_notification(FEEDBACK_REQUEST[7][0], order)
            return HttpResponse(json.dumps({'message': 'Сообщение было успешно отправлено.<br>Спасибо!'}), content_type="application/json")
        else:
            return super(ContactUsView, self).form_valid(form)

    def form_invalid(self, form):
        if self.request.is_ajax():
            return HttpResponseServerError(form.errors.as_json(), content_type='application/json')
        else:
            return super(ContactUsView, self).form_invalid(form)


class FaqView(FormView):
    form_class = ContactUsForm

    def form_valid(self, form):
        if self.request.is_ajax():
            order = 'Имя      -    {0}\n' \
                    'Email    -    {1}\n' \
                    'Текст вопроса:\n{2}'.format(form.cleaned_data['name'],
                                                   form.cleaned_data['email'],
                                                   form.cleaned_data['message'])
            FormRequest(type=FEEDBACK_REQUEST[8][0],
                        name=form.cleaned_data['name'],
                        email=form.cleaned_data['email'],
                        order=order).save()
            form_email_notification(FEEDBACK_REQUEST[8][0], order)
            return HttpResponse(json.dumps({'message': 'Спасибо за вопрос.<br>Ответим в ближайшее время.'}), content_type="application/json")
        else:
            return super(FaqView, self).form_valid(form)

    def form_invalid(self, form):
        if self.request.is_ajax():
            return HttpResponseServerError(form.errors.as_json(), content_type='application/json')
        else:
            return super(FaqView, self).form_invalid(form)