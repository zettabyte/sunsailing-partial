from django.db import models



FEEDBACK_REQUEST = (('Calculate yacht charter', 'Calculate yacht charter'),
                    ('Bareboat yacht charter', 'Bareboat yacht charter'),
                    ('Crewed yacht charter', 'Crewed yacht charter'),
                    ('Cabin charter', 'Cabin charter'),
                    ('School theory', 'School theory'),
                    ('School cruise', 'School cruise'),
                    ('Yacht catalogue', 'Yacht catalogue'),
                    ('Contact us', 'Contact us'),
                    ('Ask question', 'Ask question'))


class FeedbackTypeRequest(models.Model):
    type = models.CharField(max_length=255, choices=FEEDBACK_REQUEST, unique=True)
    text_top = models.TextField(null=True, blank=True)
    text_bottom = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.type


class FormRequest(models.Model):
    type = models.CharField(max_length=255, choices=FEEDBACK_REQUEST, null=True)
    name = models.CharField(max_length=255)
    phone = models.CharField(max_length=255)
    email = models.EmailField()
    order = models.TextField()
    submission_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('-submission_date',)