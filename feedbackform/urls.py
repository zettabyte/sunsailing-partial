from django.conf.urls import url

from .views import *



urlpatterns = [
    url(r'^proschitat-tur-na-yahte/$', TourCalculationView.as_view(), name='tour_calculation'),
    url(r'^bareboat-yacht-charter/(?P<price_id>[0-9]+)/$', BareboatYachtCharterView.as_view(), name='bareboat_yacht_charter'),
    url(r'^crewed-yacht-charter/(?P<price_id>[0-9]+)/$', CrewedYachtCharterView.as_view(), name='crewed_yacht_charter'),
    url(r'^cabin-yacht-charter/(?P<price_id>[0-9]+)/$', CabinCharterView.as_view(), name='cabin_charter'),
    url(r'^school-theory/$', SchoolTheoryView.as_view(), name='school_theory'),
    url(r'^school-cruise/(?P<price_id>[0-9]+)/$', SchoolCruiseView.as_view(), name='school_cruise'),
    url(r'^yacht-catalogue/(?P<yacht_id>[0-9]+)/$', YachtCatalogueView.as_view(), name='yacht_catalogue'),
    url(r'^contact-us/$', ContactUsView.as_view(), name='contact_us'),
    url(r'^faq/$', FaqView.as_view(), name='faq'),
    url(r'^thankyou/(?P<order_id>[0-9]+)/$', ThankYouView.as_view(), name='thankyou'),
    ]
