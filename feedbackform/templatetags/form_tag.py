from django import template
from feedbackform.forms import ContactUsForm, FaqForm
register = template.Library()


@register.assignment_tag
def contact_us_form():
    return ContactUsForm()

@register.assignment_tag
def faq_form():
    return FaqForm()