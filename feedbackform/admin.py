from django.contrib import admin

from .models import *



class FormRequestAdmin(admin.ModelAdmin):
     list_display = ('type', 'name', 'phone', 'email', 'submission_date')

     def has_add_permission(self, request, obj=None):
         return False

     def save_model(self, request, obj, form, change):
         pass



admin.site.register(FormRequest, FormRequestAdmin)
admin.site.register(FeedbackTypeRequest)