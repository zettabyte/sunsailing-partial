from django.apps import AppConfig



class MenuConfig(AppConfig):
    name = 'menu'
    verbose_name = 'Menu'

    def ready(self):
        import menu.signals