from django.db import models



class MenuItem(models.Model):
    parent = models.ForeignKey('self', null=True, blank=True)
    title = models.CharField(max_length=255)
    banner_title = models.CharField(max_length=255, null=True, blank=True)
    banner_subtitle = models.CharField(max_length=255, null=True, blank=True)
    banner_image = models.ImageField(upload_to='menu/menuitem/banner_image/', null=True, blank=True)
    breadcrumbs_title = models.CharField(max_length=255, null=True, blank=True)
    url = models.CharField(max_length=255, null=True, blank=True, unique=True)
    order_id = models.PositiveIntegerField(null=True, blank=True)

    def get_absolute_url(self):
        return self.url

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        if self.url == "":
            self.url = None
        super(MenuItem, self).save(*args, **kwargs)

    class Meta:
        ordering = ('order_id',)
