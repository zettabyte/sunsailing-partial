# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('menu', '0006_auto_20150710_1338'),
    ]

    operations = [
        migrations.AlterField(
            model_name='menuitem',
            name='banner_image',
            field=models.ImageField(blank=True, null=True, upload_to='menu/'),
        ),
    ]
