# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('menu', '0003_auto_20150709_1444'),
    ]

    operations = [
        migrations.AddField(
            model_name='menuitem',
            name='breadcrumbs_title',
            field=models.CharField(max_length=255, null=True),
        ),
    ]
