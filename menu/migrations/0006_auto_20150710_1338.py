# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('menu', '0005_auto_20150709_1951'),
    ]

    operations = [
        migrations.AddField(
            model_name='menuitem',
            name='banner_image',
            field=models.ImageField(null=True, upload_to='menu/'),
        ),
        migrations.AddField(
            model_name='menuitem',
            name='banner_subtitle',
            field=models.CharField(null=True, max_length=255, blank=True),
        ),
        migrations.AddField(
            model_name='menuitem',
            name='banner_title',
            field=models.CharField(null=True, max_length=255, blank=True),
        ),
    ]
