# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('menu', '0004_menuitem_breadcrumbs_title'),
    ]

    operations = [
        migrations.AlterField(
            model_name='menuitem',
            name='breadcrumbs_title',
            field=models.CharField(max_length=255, blank=True, null=True),
        ),
    ]
