# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('menu', '0011_auto_20150712_1536'),
    ]

    operations = [
        migrations.AlterField(
            model_name='menuitem',
            name='banner_image',
            field=models.ImageField(null=True, upload_to='menu/menuitem/banner_image/', blank=True),
        ),
    ]
