# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='MenuItem',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('title', models.CharField(max_length=255)),
                ('url', models.URLField()),
                ('order_id', models.PositiveIntegerField(null=True, blank=True)),
                ('parent', models.ForeignKey(null=True, to='menu.MenuItem', blank=True)),
            ],
            options={
                'ordering': ('order_id',),
            },
        ),
    ]
