from django.contrib import admin

from utils import image_widgets

from .models import *



class MenuItemAdmin(admin.ModelAdmin):
    save_on_top = True
    list_display = ('title', 'parent', 'url', 'order_id')
    list_editable = ('order_id',)
    list_filter = ('parent',)
    formfield_overrides = {models.ImageField: {'widget': image_widgets.AdminImagePreviewWidget}}



admin.site.register(MenuItem, MenuItemAdmin)