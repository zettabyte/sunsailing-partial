from django import template
from menu.models import MenuItem
from django.core.urlresolvers import reverse


register = template.Library()

def build_breadcrumbs(menu_path, *args):
    last_item = MenuItem.objects.get(url=menu_path)
    item_list = [{'url':last_item.url, 'title':last_item.breadcrumbs_title}]
    iter_list = iter(args)
    while True:
        try:
            item_url = next(iter_list)
            item_title = next(iter_list)
            item_list.append({'url':item_url, 'title':item_title})
        except StopIteration:
            break
    return item_list


@register.inclusion_tag('menu/breadcrumbs.html')
def breadcrumbs_show(path, *args, **kwargs):
    if 'abs_url' in kwargs and kwargs.pop('abs_url'):
        url = path
    else:
        url = reverse(path)
    return {'breadcrumbs_list': build_breadcrumbs((url), *args)}


@register.simple_tag
def menu_banner_image(path, abs_url=False):
    if abs_url:
        url = path
    else:
        url = reverse(path)
    return MenuItem.objects.get(url=url).banner_image.url


@register.simple_tag
def menu_banner_title(path, abs_url=False):
    if abs_url:
        url = path
    else:
        url = reverse(path)
    return MenuItem.objects.get(url=url).banner_title


@register.simple_tag
def menu_banner_subtitle(path, abs_url=False):
    if abs_url:
        url = path
    else:
        url = reverse(path)
    return MenuItem.objects.get(url=url).banner_subtitle