from django import template
from menu.models import MenuItem


register = template.Library()


@register.assignment_tag
def main_menu_show(current_path, spaces=0):
    all_items = MenuItem.objects.values()
    top_level_items = [i for i in all_items if i.get('parent_id')==None]
    return build_menu(top_level_items, all_items, current_path, spaces)['res_str']


def build_menu(item_list, all_items, path, spaces=0):
    """
    In general - algorithm makes HTML list from recursive relation in model.
    So, this algorithm builds HTML menu list from recursive relation of menu items from model with Bootstrap CSS classes and other stuff.
    Also, it calculates hierarchy path of active menu item.
    :param item_list: Top level items which don't have parents
    :param all_items: All items in tree
    :param path: URL path of current page. Need to add CSS class="active"
    :param tabs: Uses in recursive calls for formatting HTML list
    :return:
    """
    indent = ' ' * spaces
    res = '\n'
    has_active = False
    for item in item_list:
        child_list = [i for i in all_items if i.get('parent_id')==item.get('id')]
        if child_list:
            value_list=build_menu(child_list, all_items, path, spaces + 4)
            has_active = has_active or value_list['has_active']
            res += '{tab}'.format(tab=indent)
            if value_list['has_active']:
                res += '<li class="dropdown active">'
            else:
                res += '<li class="dropdown">'
            res += '<a class="dropdown-toggle" data-toggle="dropdown" href="{url}">'.format(url=item.get('url') if item.get('url') else '#')
            res += item.get('title')
            res += '</a>\n{tab}'.format(tab=indent)
            res += '<ul class="dropdown-menu">{value_list}{tab}</ul>\n'.format(value_list=value_list['res_str'], tab=indent)
            res += '{tab}</li>\n'.format(tab=indent)
        else:
            res += '{tab}'.format(tab=indent)
            if item.get('url') and item.get('url') in path:
                has_active = True
                res += '<li class="active">'
            else:
                res += '<li>'
            res += '<a href="{url}">{title}</a></li>\n'.format(url=item.get('url') if item.get('url') else '#', title=item.get('title'), tab=indent)
    return {'res_str':res, 'has_active':has_active}
