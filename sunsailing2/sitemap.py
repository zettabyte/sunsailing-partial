from django.contrib.sitemaps import Sitemap
from django.core.urlresolvers import reverse
from yachtcharter import models as yachtcharter_models
from cabincharter import models as cabincharter_models
from school import models as school_models
from countryhandbook import models as countryhdbk_models


class YachtCharterStaticSitemap(Sitemap):
    priority = 0.5
    changefreq = 'never'

    def items(self):
        return ['yachtcharter:main']

    def location(self, item):
        return reverse(item)


class YachtSitemap(Sitemap):
    changefreq = "monthly"
    priority = 0.5

    def items(self):
        return yachtcharter_models.Yacht.objects.all()


class CountryCharterSitemap(Sitemap):
    changefreq = "weekly"
    priority = 1.0

    def items(self):
        return yachtcharter_models.CountryCharter.objects.all()


class YachtTypeSitemap(Sitemap):
    changefreq = "monthly"
    priority = 0.5

    def items(self):
        return yachtcharter_models.YachtType.objects.all()


class CabinCharterStaticSitemap(Sitemap):
    priority = 0.7
    changefreq = 'monthly'

    def items(self):
        return ['cabincharter:main']

    def location(self, item):
        return reverse(item)


class CabinCharterCurrentOfferSitemap(Sitemap):
    changefreq = "weekly"
    priority = 0.9

    def items(self):
        return cabincharter_models.Offer.current_objects.all()


class CabinCharterArchiveOfferSitemap(Sitemap):
    changefreq = "monthly"
    priority = 0.5

    def items(self):
        return cabincharter_models.Offer.archive_objects.all()
    
    
class SchoolCruiseListStaticSitemap(Sitemap):
    priority = 0.7
    changefreq = 'monthly'

    def items(self):
        return ['school:cruise_list']

    def location(self, item):
        return reverse(item)


class SchoolTheorySitemap(Sitemap):
    priority = 1.0
    changefreq = 'monthly'

    def items(self):
        return school_models.Theory.objects.all()


class SchoolCruiseSitemap(Sitemap):
    changefreq = "monthly"
    priority = 0.9

    def items(self):
        return school_models.Cruise.objects.all()


class CountryHdbkStaticSitemap(Sitemap):
    priority = 0.7
    changefreq = 'monthly'

    def items(self):
        return ['countryhandbook:main']

    def location(self, item):
        return reverse(item)


class CountryHdbkSitemap(Sitemap):
    changefreq = "monthly"
    priority = 0.9

    def items(self):
        return countryhdbk_models.Country.objects.all()


class FaqStaticSitemap(Sitemap):
    priority = 0.5
    changefreq = 'monthly'

    def items(self):
        return ['faq:main']

    def location(self, item):
        return reverse(item)
