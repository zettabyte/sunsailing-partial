from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.flatpages import views

from django.conf import settings
from django.conf.urls.static import static

from django.views.generic import TemplateView
from django.contrib.sitemaps.views import sitemap

from django.contrib.flatpages.sitemaps import FlatPageSitemap

from sunsailing2.sitemap import *


sitemaps = {
    'yachtcharterstaticview': YachtCharterStaticSitemap,
    'yachts': YachtSitemap,
    'countrycharter': CountryCharterSitemap,
    'yachttype': YachtTypeSitemap,

    'cabincharterstatic': CabinCharterStaticSitemap,
    'cabinchartercurrentoffer': CabinCharterCurrentOfferSitemap,
    'cabincharterarchiveoffer': CabinCharterArchiveOfferSitemap,

    'schoolcruisestatic': SchoolCruiseListStaticSitemap,
    'schooltheory': SchoolTheorySitemap,
    'schoolcruise': SchoolCruiseSitemap,

    'countryhdbkstatic': CountryHdbkStaticSitemap,
    'countryhdbk': CountryHdbkSitemap,

    'faqstatic': FaqStaticSitemap,

    'flatpages': FlatPageSitemap
}


urlpatterns = [
                  url(r'^admin/', include(admin.site.urls)),
                  url(r'^sitemap\.xml$', sitemap, {'sitemaps': sitemaps}, name='django.contrib.sitemaps.views.sitemap'),
                  url(r'^robots\.txt$', TemplateView.as_view(template_name='robots.txt', content_type='text/plain')),
                  url(r'^redactor/', include('redactor.urls')),


                  url(r'^blog/', include('blog.urls', namespace='blog')),
                  url(r'^kruizy-na-yahte/', include('cabincharter.urls', namespace='cabincharter')),
                  url(r'^shkola-yahtinga/', include('school.urls', namespace='school')),
                  url(r'^arenda-yaht/', include('yachtcharter.urls', namespace='yachtcharter')),
                  url(r'^strany-dlya-yahtinga/', include('countryhandbook.urls', namespace='countryhandbook')),
                  url(r'^voprosy-i-otvety/', include('faq.urls', namespace='faq')),
                  url(r'^otpravit-zayavku/', include('feedbackform.urls', namespace='feedbackform')),
                  url(r'^$', views.flatpage, {'url': '/'}, name='home'),
                  url(r'^kontakty/$', views.flatpage, {'url': '/kontakty/'}, name='contacts'),
                  url(r'^photobank/', include('photobank.urls', namespace='photobank')),
              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
