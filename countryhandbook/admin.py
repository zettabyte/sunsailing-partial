from django.contrib import admin

from easy_select2 import select2_modelform
from redactor.widgets import RedactorEditor
from utils import image_widgets

from .models import *



class RegionAdmin(admin.ModelAdmin):
    list_display = ('title', 'order_id')
    list_editable = ('order_id',)
    save_on_top = True


class CountryAdmin(admin.ModelAdmin):
    list_display = ('title', 'order_id')
    list_editable = ('order_id',)
    prepopulated_fields = {'slug': ('title',)}
    list_filter = ('region',)
    form = select2_modelform(Country, attrs={'width': '250px'})
    save_on_top = True
    formfield_overrides = {models.ImageField: {'widget': image_widgets.AdminImagePreviewWidget}}


class CharterEntryInline(admin.StackedInline):
    model = CharterEntry
    form = select2_modelform(CharterEntry, attrs={'width': '250px'})
    formfield_overrides = {models.TextField: {'widget': RedactorEditor()}}
    extra = 0


class CharterAdmin(admin.ModelAdmin):
    prepopulated_fields = {'menu_slug': ('menu_title',)}
    form = select2_modelform(Charter, attrs={'width': '250px'})
    inlines = [CharterEntryInline]
    list_filter = ('country__title',)
    save_on_top = True


class YachtingAdmin(admin.ModelAdmin):
    prepopulated_fields = {'menu_slug': ('menu_title',)}
    list_filter = ('country__title',)
    form = select2_modelform(Yachting, attrs={'width': '250px'})
    formfield_overrides = {models.TextField: {'widget': RedactorEditor()}}
    save_on_top = True


class InfoEntryInline(admin.StackedInline):
    model = InfoEntry
    extra = 0
    formfield_overrides = {models.TextField: {'widget': RedactorEditor(redactor_options={'buttonsHide': ['file', 'image']})}}


class InfoAdmin(admin.ModelAdmin):
    prepopulated_fields = {'menu_slug': ('menu_title',)}
    form = select2_modelform(Info, attrs={'width': '250px'})
    inlines = [InfoEntryInline]
    list_filter = ('country__title',)
    save_on_top = True


class MarinaEntryInline(admin.StackedInline):
    model = MarinaEntry
    extra = 0
    formfield_overrides = {models.ImageField: {'widget': image_widgets.AdminImagePreviewWidget},
                           models.TextField: {'widget': RedactorEditor()}}


class MarinaAdmin(admin.ModelAdmin):
    prepopulated_fields = {'menu_slug': ('menu_title',)}
    form = select2_modelform(Marina, attrs={'width': '250px'})
    inlines = [MarinaEntryInline]
    list_filter = ('country__title',)
    save_on_top = True


class RouteAdmin(admin.ModelAdmin):
    prepopulated_fields = {'menu_slug': ('menu_title',)}
    form = select2_modelform(Route, attrs={'width': '250px'})
    list_filter = ('country__title',)
    save_on_top = True


class RouteEntryDayInline(admin.TabularInline):
    model = RouteEntryDay
    extra = 0
    formfield_overrides = {models.ImageField: {'widget': image_widgets.AdminImagePreviewWidget},
                           models.TextField: {'widget': RedactorEditor(redactor_options={'minHeight': 70})}}


class RouteEntryAdmin(admin.ModelAdmin):
    form = select2_modelform(RouteEntry, attrs={'width': '250px'})
    inlines = [RouteEntryDayInline]
    list_filter = ('route__country__title',)
    save_on_top = True


class ShowplaceEntryInline(admin.StackedInline):
    model = ShowplaceEntry
    extra = 0
    formfield_overrides = {models.ImageField: {'widget': image_widgets.AdminImagePreviewWidget},
                           models.TextField: {'widget': RedactorEditor(redactor_options={'minHeight': 70})}}


class ShowplaceAdmin(admin.ModelAdmin):
    form = select2_modelform(Showplace, attrs={'width': '250px'})
    prepopulated_fields = {'menu_slug': ('menu_title',)}
    inlines = [ShowplaceEntryInline]
    list_filter = ('country__title',)
    save_on_top = True



admin.site.register(Region, RegionAdmin)
admin.site.register(Country, CountryAdmin)
admin.site.register(Charter, CharterAdmin)
admin.site.register(Yachting, YachtingAdmin)
admin.site.register(Info, InfoAdmin)
admin.site.register(Marina, MarinaAdmin)
admin.site.register(Route, RouteAdmin)
admin.site.register(RouteEntry, RouteEntryAdmin)
admin.site.register(Showplace, ShowplaceAdmin)