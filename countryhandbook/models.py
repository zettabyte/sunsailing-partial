import os

from django.db import models
from django.core.urlresolvers import reverse
from django.contrib.contenttypes.fields import GenericRelation

from utils import img_utils



class Region(models.Model):
    title = models.CharField(max_length=255)
    order_id = models.PositiveIntegerField(blank=True, null=True)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ('order_id',)


class Country(models.Model):
    region = models.ForeignKey(Region)
    title = models.CharField(max_length=255)
    slug = models.SlugField(unique=True)
    subtitle = models.CharField(max_length=255, null=True)
    banner_image = models.ImageField(upload_to='countryhandbook/country/banner_image/')
    card_image = models.ImageField(upload_to='countryhandbook/country/card_image/', help_text='Auto resize to 740px')
    order_id = models.PositiveIntegerField(blank=True, null=True)

    def save(self, *args, **kwargs):
        if not os.path.isfile(self.card_image.path):
            self.card_image = img_utils.resize_ratio(740, self.card_image.file)
        super(Country, self).save(*args, **kwargs)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('countryhandbook:country_detail', args=[self.slug])

    class Meta:
        ordering = ('order_id',)


class CountrySection(models.Model):
    country = models.OneToOneField(Country)
    title = models.CharField(max_length=255)
    menu_title = models.CharField(max_length=255)
    menu_slug = models.SlugField()

    def __str__(self):
        return self.title + ' | ' + self.country.title

    class Meta:
        abstract = True


class Charter(CountrySection):
    pass


class CharterEntry(models.Model):
    charter = models.ForeignKey(Charter)
    country_charter = models.OneToOneField('yachtcharter.CountryCharter')
    media_heading = models.CharField(max_length=255)
    media_body = models.TextField()
    order_id = models.PositiveIntegerField(blank=True, null=True)

    class Meta:
        ordering = ('order_id',)


class Yachting(CountrySection):
    entry_text = models.TextField()


class Info(CountrySection):
    pass


class InfoEntry(models.Model):
    info = models.ForeignKey(Info)
    title = models.CharField(max_length=255)
    entry_text = models.TextField()
    order_id = models.PositiveIntegerField(blank=True, null=True)

    class Meta:
        ordering = ('order_id',)


class Marina(CountrySection):
    pass


class MarinaEntry(models.Model):
    marina = models.ForeignKey(Marina)
    title = models.CharField(max_length=255)
    image = models.ImageField(upload_to='countryhandbook/marina/', help_text='Auto resize to 850px')
    map_link = models.URLField(max_length=255)
    plan = models.FileField(upload_to='countryhandbook/marina/plans/', null=True, blank=True)
    location = models.TextField()
    contacts = models.TextField()
    entry_text = models.TextField()
    order_id = models.PositiveIntegerField(blank=True, null=True)

    def save(self, *args, **kwargs):
        if not os.path.isfile(self.image.path):
            self.image = img_utils.resize_ratio(850, self.image.file)
        super(MarinaEntry, self).save(*args, **kwargs)

    class Meta:
        ordering = ('order_id',)


class Route(CountrySection):
    pass


class RouteEntry(models.Model):
    route = models.ForeignKey(Route)
    title = models.CharField(max_length=255)
    start_port = models.CharField(max_length=255)
    length = models.PositiveIntegerField()
    map = models.ImageField(upload_to='countryhandbook/route/', help_text='Auto resize to 850px')
    map_url = models.URLField(blank=True, null=True)
    order_id = models.PositiveIntegerField(blank=True, null=True)

    def __str__(self):
        return self.title + ' | ' + self.route.country.title

    def save(self, *args, **kwargs):
        if not os.path.isfile(self.map.path):
            self.map = img_utils.resize_ratio(850, self.map.file)
        super(RouteEntry, self).save(*args, **kwargs)

    class Meta:
        ordering = ('order_id',)


class RouteEntryDay(models.Model):
    entry = models.ForeignKey(RouteEntry)
    checkpoint = models.TextField()
    length = models.PositiveIntegerField(blank=True, null=True)
    order_id = models.PositiveIntegerField(blank=True, null=True)

    class Meta:
        ordering = ('order_id',)


class Showplace(CountrySection):
    pass


class ShowplaceEntry(models.Model):
    showplace = models.ForeignKey(Showplace)
    title = models.CharField(max_length=255)
    image = models.ImageField(upload_to='countryhandbook/showplace/', help_text='Auto resize to 850px')
    address = models.TextField()
    opening_hours = models.TextField(null=True, blank=True)
    ticket_price = models.TextField(null=True, blank=True)
    recommend = models.TextField(null=True, blank=True)
    entry_text = models.TextField()
    order_id = models.PositiveIntegerField(blank=True, null=True)

    def save(self, *args, **kwargs):
        if not os.path.isfile(self.image.path):
            self.image = img_utils.resize_ratio(850, self.image.file)
        super(ShowplaceEntry, self).save(*args, **kwargs)

    class Meta:
        ordering = ('order_id',)