# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('countryhandbook', '0005_auto_20150728_1530'),
    ]

    operations = [
        migrations.AlterField(
            model_name='marinaentry',
            name='image',
            field=models.ImageField(help_text='Auto resize to 850px', upload_to='countryhandbook/marina/'),
        ),
        migrations.AlterField(
            model_name='routeentry',
            name='map',
            field=models.ImageField(help_text='Auto resize to 850px', upload_to='countryhandbook/route/'),
        ),
        migrations.AlterField(
            model_name='showplaceentry',
            name='image',
            field=models.ImageField(help_text='Auto resize to 850px', upload_to='countryhandbook/showplace/'),
        ),
    ]
