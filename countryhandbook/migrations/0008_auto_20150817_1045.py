# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('countryhandbook', '0007_auto_20150815_2219'),
    ]

    operations = [
        migrations.AlterField(
            model_name='routeentryday',
            name='checkpoint',
            field=models.TextField(),
        ),
    ]
