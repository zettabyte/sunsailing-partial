# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('yachtcharter', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Charter',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('menu_title', models.CharField(max_length=255)),
                ('menu_slug', models.SlugField()),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='CharterEntry',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('media_heading', models.CharField(max_length=255)),
                ('media_body', models.TextField()),
                ('order_id', models.PositiveIntegerField(null=True, blank=True)),
                ('charter', models.ForeignKey(to='countryhandbook.Charter')),
                ('country_charter', models.OneToOneField(to='yachtcharter.CountryCharter')),
            ],
            options={
                'ordering': ('order_id',),
            },
        ),
        migrations.CreateModel(
            name='Country',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('slug', models.SlugField(unique=True)),
                ('image', models.ImageField(upload_to='countryhandbook/country/headers/')),
                ('thumbnail_image', models.ImageField(help_text='Auto resize to 330px', upload_to='countryhandbook/country/thumbnails/')),
                ('order_id', models.PositiveIntegerField(null=True, blank=True)),
            ],
            options={
                'ordering': ('order_id',),
            },
        ),
        migrations.CreateModel(
            name='Info',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('menu_title', models.CharField(max_length=255)),
                ('menu_slug', models.SlugField()),
                ('country', models.OneToOneField(to='countryhandbook.Country')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='InfoEntry',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('entry_text', models.TextField()),
                ('order_id', models.PositiveIntegerField(null=True, blank=True)),
                ('info', models.ForeignKey(to='countryhandbook.Info')),
            ],
            options={
                'ordering': ('order_id',),
            },
        ),
        migrations.CreateModel(
            name='Marina',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('menu_title', models.CharField(max_length=255)),
                ('menu_slug', models.SlugField()),
                ('country', models.OneToOneField(to='countryhandbook.Country')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='MarinaEntry',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('image', models.ImageField(upload_to='countryhandbook/marina/')),
                ('map_link', models.URLField(max_length=255)),
                ('plan', models.FileField(null=True, upload_to='countryhandbook/marina/plans/', blank=True)),
                ('location', models.TextField()),
                ('contacts', models.TextField()),
                ('entry_text', models.TextField()),
                ('order_id', models.PositiveIntegerField(null=True, blank=True)),
                ('marina', models.ForeignKey(to='countryhandbook.Marina')),
            ],
            options={
                'ordering': ('order_id',),
            },
        ),
        migrations.CreateModel(
            name='Region',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('order_id', models.PositiveIntegerField(null=True, blank=True)),
            ],
            options={
                'ordering': ('order_id',),
            },
        ),
        migrations.CreateModel(
            name='Route',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('menu_title', models.CharField(max_length=255)),
                ('menu_slug', models.SlugField()),
                ('country', models.OneToOneField(to='countryhandbook.Country')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='RouteEntry',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('start_port', models.CharField(max_length=255)),
                ('length', models.PositiveIntegerField()),
                ('map', models.ImageField(help_text='Auto resize to 750px', upload_to='countryhandbook/route/')),
                ('map_url', models.URLField(null=True, blank=True)),
                ('order_id', models.PositiveIntegerField(null=True, blank=True)),
                ('route', models.ForeignKey(to='countryhandbook.Route')),
            ],
            options={
                'ordering': ('order_id',),
            },
        ),
        migrations.CreateModel(
            name='RouteEntryDay',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('checkpoint', models.CharField(max_length=255)),
                ('length', models.PositiveIntegerField()),
                ('order_id', models.PositiveIntegerField(null=True, blank=True)),
                ('entry', models.ForeignKey(to='countryhandbook.RouteEntry')),
            ],
            options={
                'ordering': ('order_id',),
            },
        ),
        migrations.CreateModel(
            name='Showplace',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('menu_title', models.CharField(max_length=255)),
                ('menu_slug', models.SlugField()),
                ('country', models.OneToOneField(to='countryhandbook.Country')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ShowplaceEntry',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('image', models.ImageField(upload_to='countryhandbook/showplace/')),
                ('address', models.TextField()),
                ('opening_hours', models.TextField(null=True, blank=True)),
                ('ticket_price', models.TextField(null=True, blank=True)),
                ('recommend', models.TextField(null=True, blank=True)),
                ('entry_text', models.TextField()),
                ('order_id', models.PositiveIntegerField(null=True, blank=True)),
                ('showplace', models.ForeignKey(to='countryhandbook.Showplace')),
            ],
            options={
                'ordering': ('order_id',),
            },
        ),
        migrations.CreateModel(
            name='Yachting',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('menu_title', models.CharField(max_length=255)),
                ('menu_slug', models.SlugField()),
                ('entry_text', models.TextField()),
                ('country', models.OneToOneField(to='countryhandbook.Country')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='country',
            name='region',
            field=models.ForeignKey(to='countryhandbook.Region'),
        ),
        migrations.AddField(
            model_name='charter',
            name='country',
            field=models.OneToOneField(to='countryhandbook.Country'),
        ),
    ]
