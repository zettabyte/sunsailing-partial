# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('countryhandbook', '0004_country_subtitle'),
    ]

    operations = [
        migrations.AlterField(
            model_name='routeentryday',
            name='length',
            field=models.PositiveIntegerField(blank=True, null=True),
        ),
    ]
