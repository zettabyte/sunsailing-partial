# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('countryhandbook', '0002_auto_20150707_1424'),
    ]

    operations = [
        migrations.RenameField(
            model_name='country',
            old_name='image',
            new_name='banner_image',
        ),
        migrations.RenameField(
            model_name='country',
            old_name='thumbnail_image',
            new_name='card_image',
        ),
    ]
