# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('countryhandbook', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='country',
            name='thumbnail_image',
            field=models.ImageField(help_text='Auto resize to 740px', upload_to='countryhandbook/country/thumbnails/'),
        ),
    ]
