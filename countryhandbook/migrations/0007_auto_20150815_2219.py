# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('countryhandbook', '0006_auto_20150728_1639'),
    ]

    operations = [
        migrations.AlterField(
            model_name='country',
            name='banner_image',
            field=models.ImageField(upload_to='countryhandbook/country/banner_image/'),
        ),
        migrations.AlterField(
            model_name='country',
            name='card_image',
            field=models.ImageField(help_text='Auto resize to 740px', upload_to='countryhandbook/country/card_image/'),
        ),
    ]
