from django.views.generic.list import ListView
from django.views.generic.detail import DetailView

from .models import Country, Region


class CountryList(ListView):
    queryset = Region.objects.prefetch_related('country_set').all()
    context_object_name = 'region_list'
    template_name = 'countryhandbook/country_list.html'


class CountryDetail(DetailView):
    queryset = Country.objects.prefetch_related('charter__charterentry_set__country_charter__yacht_type').all()
    context_object_name = 'country'
    template_name = 'countryhandbook/country_detail.html'