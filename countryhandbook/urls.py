from django.conf.urls import url

from .views import *


urlpatterns = [
    url(r'^$', CountryList.as_view(), name = 'main'),
    url(r'^(?P<slug>[\w-]+)/$', CountryDetail.as_view(), name='country_detail'),
    ]