from django.apps import AppConfig



class CountryHandbookConfig(AppConfig):
    name = 'countryhandbook'
    verbose_name = 'Country Handbook'

    def ready(self):
        import countryhandbook.signals