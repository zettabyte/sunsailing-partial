from django.db.models.signals import pre_save, pre_delete
from django.dispatch import receiver

from utils import signal_utils

from .models import Country, MarinaEntry, RouteEntry, ShowplaceEntry



@receiver(pre_save, sender=Country)
@receiver(pre_save, sender=MarinaEntry)
@receiver(pre_save, sender=RouteEntry)
@receiver(pre_save, sender=ShowplaceEntry)
def del_exist_img(instance, sender, **kwargs):
    signal_utils.del_file_on_replace_item(instance, sender)


@receiver(pre_delete, sender=Country)
@receiver(pre_delete, sender=MarinaEntry)
@receiver(pre_delete, sender=RouteEntry)
@receiver(pre_delete, sender=ShowplaceEntry)
def del_item(instance, **kwargs):
    signal_utils.del_file_on_del_item(instance)