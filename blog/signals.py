from django.db.models.signals import pre_save, pre_delete
from django.dispatch import receiver

from utils import signal_utils

from .models import Post



@receiver(pre_save, sender=Post)
def del_exist_img(instance, sender, **kwargs):
    signal_utils.del_file_on_replace_item(instance, sender)


@receiver(pre_delete, sender=Post)
def del_item(instance, **kwargs):
    signal_utils.del_file_on_del_item(instance)