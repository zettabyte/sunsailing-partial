# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0009_remove_post_sub_title'),
    ]

    operations = [
        migrations.RenameField(
            model_name='post',
            old_name='image',
            new_name='banner_image',
        ),
        migrations.RenameField(
            model_name='post',
            old_name='thumbnail_image',
            new_name='card_image',
        ),
    ]
