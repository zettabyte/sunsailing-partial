# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0014_auto_20150827_2029'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='body',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='post',
            name='introtext',
            field=models.TextField(null=True, blank=True),
        ),
    ]
