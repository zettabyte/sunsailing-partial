# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0004_auto_20150706_1422'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='category',
            options={'ordering': ('order_id',)},
        ),
        migrations.AddField(
            model_name='category',
            name='order_id',
            field=models.PositiveIntegerField(blank=True, null=True),
        ),
    ]
