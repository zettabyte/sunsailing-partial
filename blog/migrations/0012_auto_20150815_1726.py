# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0011_auto_20150728_1530'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='banner_image',
            field=models.ImageField(upload_to='blog/post/banner_image/'),
        ),
        migrations.AlterField(
            model_name='post',
            name='card_image',
            field=models.ImageField(help_text='Auto resize to 750px', null=True, upload_to='blog/post/card_image/'),
        ),
    ]
