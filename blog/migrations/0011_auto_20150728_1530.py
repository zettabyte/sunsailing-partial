# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0010_auto_20150710_1639'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='card_image',
            field=models.ImageField(upload_to='blog/thumbnails/', null=True, help_text='Auto resize to 750px'),
        ),
    ]
