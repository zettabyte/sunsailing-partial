# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0005_auto_20150706_1424'),
    ]

    operations = [
        migrations.AddField(
            model_name='post',
            name='thumbnail_image',
            field=models.ImageField(upload_to='blog/thumbnails/', help_text='Auto resize to 330px', null=True),
        ),
    ]
