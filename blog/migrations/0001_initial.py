# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('image', models.ImageField(upload_to='blog/headers/')),
                ('body', models.TextField()),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('publish_date', models.DateTimeField(default=django.utils.timezone.now)),
                ('publish', models.BooleanField(default=True)),
                ('slug', models.SlugField(unique=True)),
                ('category', models.ForeignKey(to='blog.Category')),
            ],
        ),
    ]
