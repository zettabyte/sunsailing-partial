# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0012_auto_20150815_1726'),
    ]

    operations = [
        migrations.AddField(
            model_name='post',
            name='introtext',
            field=models.TextField(null=True),
        ),
    ]
