from django.shortcuts import get_list_or_404
from django.views import generic

from .models import Post, Category



class PostDetail(generic.DetailView):
    model = Post
    template_name = "blog/post.html"

    def get_context_data(self, **kwargs):
        context = super(PostDetail, self).get_context_data(**kwargs)
        context['cat_list'] = Category.objects.all()
        return context


class PostList(generic.ListView):
    queryset = Post.objects.prefetch_related('category').get_post_list()
    context_object_name = 'post_list'
    template_name = "blog/blog.html"
    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super(PostList, self).get_context_data(**kwargs)
        context['cat_list'] = Category.objects.all()
        return context

    def get_queryset(self):
        qs = super(PostList, self).get_queryset()
        if self.kwargs.get('slug'):
            qs = get_list_or_404(qs, category__slug=self.kwargs['slug'])
        return qs