from django.contrib import admin

from easy_select2 import select2_modelform
from redactor.widgets import RedactorEditor
from utils import image_widgets
from photobank.admin import SinglePhotoInline, GalleryPhotoInline

from .models import *



class CategoryAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title', )}
    list_display = ('title', 'order_id')
    list_editable = ('order_id',)
    save_on_top = True


class PostAdmin(admin.ModelAdmin):
    list_display = ('title', 'publish', 'category', 'publish_date')
    prepopulated_fields = {'slug': ('title', )}
    formfield_overrides = {models.ImageField: {'widget': image_widgets.AdminImagePreviewWidget}}
    inlines = [SinglePhotoInline, GalleryPhotoInline]
    form = select2_modelform(Post, attrs={'width': '250px'})
    save_on_top = True

    def get_form(self, request, obj=None, **kwargs):
        if obj:
            self.formfield_overrides[models.TextField] = {'widget': RedactorEditor(redactor_options={'imageManagerJson': image_widgets.single_photo_json_url(obj.id, obj.photo_single.content_type.id),
                                                                                                     'buttonsHide': ['file']})}
        else:
            self.formfield_overrides[models.TextField] = {'widget': RedactorEditor()}
        return super(PostAdmin, self).get_form(request, obj, **kwargs)



admin.site.register(Category, CategoryAdmin)
admin.site.register(Post, PostAdmin)