from django.conf.urls import url

from .views import *



urlpatterns = [
    url(r'^$', PostList.as_view(), name = 'main'),
    url(r'^category/(?P<slug>[\w-]+)/$', PostList.as_view(), name = 'category_filter'),
    url(r'^(?P<slug>[\w-]+)/$', PostDetail.as_view(), name = 'post'),
    ]