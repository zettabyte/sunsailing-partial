import os

from django.db import models
from django.utils import timezone
from django.core.urlresolvers import reverse
from django.contrib.contenttypes.fields import GenericRelation

from utils import img_utils



class Category(models.Model):
    title = models.CharField(max_length=255)
    slug = models.SlugField(unique = True)
    order_id = models.PositiveIntegerField(blank=True, null=True)

    def get_absolute_url(self):
        return reverse('blog:category_filter', args=[self.slug])
    
    def __str__(self):
        return self.title

    class Meta:
        ordering = ('order_id',)


class PostsQuerySet(models.QuerySet):
    def get_post_list(self):
        return self.filter(publish = True).order_by('-publish_date')


class Post(models.Model):
    title = models.CharField(max_length=255)
    slug = models.SlugField(unique = True)
    banner_image = models.ImageField(upload_to='blog/post/banner_image/')
    card_image = models.ImageField(upload_to='blog/post/card_image/', null=True, help_text='Auto resize to 750px')
    category = models.ForeignKey(Category)
    introtext = models.TextField(null=True, blank=True)
    body = models.TextField(null=True, blank=True)
    created = models.DateTimeField(auto_now_add = True)
    publish_date = models.DateTimeField(default = timezone.now)
    publish = models.BooleanField(default = True)
    objects = PostsQuerySet.as_manager()
    photo_single = GenericRelation('photobank.SinglePhoto')
    photo_gallery = GenericRelation('photobank.GalleryPhoto')

    def get_absolute_url(self):
        return reverse('blog:post', args=[self.slug])

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        if not os.path.isfile(self.card_image.path):
            self.card_image = img_utils.resize_ratio(750, self.card_image.file)
        super(Post, self).save(*args, **kwargs)

    class Meta:
        ordering = ('-publish_date',)