import os

from django.db import models
from django.contrib.contenttypes.fields import GenericRelation
from django.core.urlresolvers import reverse
from django.utils import dateformat
from django.conf import settings

from utils import img_utils


class Theory(models.Model):
    title = models.CharField(max_length=255)
    slug = models.SlugField()
    sidebar_title = models.CharField(max_length=255)
    text = models.TextField()
    order_id = models.IntegerField(blank = True, null = True)
    photo_single = GenericRelation('photobank.SinglePhoto')

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('school:theory_detail', args=[self.slug])

    class Meta:
        ordering = ('order_id',)


class TheoryStart(models.Model):
    start_date = models.DateField()

    def __str__(self):
        return dateformat.format(self.start_date, 'j F Y')

    class Meta:
        ordering = ('start_date',)


class Cruise(models.Model):
    title = models.CharField(max_length=255)
    slug = models.SlugField(unique = True)
    subtitle = models.CharField(max_length=255, null=True)
    banner_image = models.ImageField(upload_to='school/cruise/banner_image/')
    card_image = models.ImageField(upload_to='school/cruise/card_image/', help_text='Auto resize to 740px')

    start_port = models.CharField(max_length=255)

    cruise_description = models.TextField()
    cruise_program = models.TextField()
    afterword = models.TextField(null=True, blank=True)

    yacht = models.ForeignKey('yachtcharter.Yacht', null=True)
    cabin = models.CharField(max_length=255, null=True)
    wc = models.CharField(max_length=255, null=True)

    price_description = models.TextField(null=True, blank=True)

    order_id = models.PositiveIntegerField(blank=True, null=True)

    photo_single = GenericRelation('photobank.SinglePhoto')

    def save(self, *args, **kwargs):
        if not os.path.isfile(self.card_image.path):
            self.card_image = img_utils.resize_ratio(740, self.card_image.file)
        super(Cruise, self).save(*args, **kwargs)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('school:cruise_detail', args=[self.slug])

    class Meta:
        ordering = ('order_id',)


class Price(models.Model):
    cruise = models.ForeignKey(Cruise)
    date_from = models.DateField()
    date_to = models.DateField()
    price = models.PositiveIntegerField()

    class Meta:
        ordering = ('date_from',)


class PriceInclude(models.Model):
    offer = models.ForeignKey(Cruise)
    include = models.CharField(max_length=255)
    order_id = models.PositiveIntegerField( blank = True, null = True)

    class Meta:
        ordering = ('order_id',)


class PriceExclude(models.Model):
    offer = models.ForeignKey(Cruise)
    exclude = models.CharField(max_length=255)
    order_id = models.PositiveIntegerField( blank = True, null = True)

    class Meta:
        ordering = ('order_id',)