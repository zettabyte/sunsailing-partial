from django.conf.urls import url

from .views import *


urlpatterns = [
    url(r'^kruizy-s-obucheniem/$', CruiseList.as_view(), name = 'cruise_list'),
    url(r'^kruizy-s-obucheniem/(?P<slug>[\w-]+)/$', CruiseDetail.as_view(), name = 'cruise_detail'),
    url(r'^(?P<slug>[\w-]+)/$', TheoryView.as_view(), name = 'theory_detail'),
    ]