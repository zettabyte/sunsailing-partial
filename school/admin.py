from django.contrib import admin

from redactor.widgets import RedactorEditor
from easy_select2 import select2_modelform
from utils import image_widgets
from photobank.admin import SinglePhotoInline

from .models import *


class TheoryAdmin(admin.ModelAdmin):
    list_display = ('title', 'order_id')
    list_editable = ('order_id',)
    inlines = [SinglePhotoInline]
    prepopulated_fields = {'slug': ('title', )}
    save_on_top = True

    def get_form(self, request, obj=None, **kwargs):
        if obj:
            self.formfield_overrides[models.TextField] = {'widget': RedactorEditor(redactor_options={'imageManagerJson': image_widgets.single_photo_json_url(obj.id, obj.photo_single.content_type.id),
                                                                                                     'buttonsHide': ['file']})}
        else:
            self.formfield_overrides[models.TextField] = {'widget': RedactorEditor()}
        return super(TheoryAdmin, self).get_form(request, obj, **kwargs)


class PriceIncludeInline(admin.TabularInline):
    model = PriceInclude
    extra = 0


class PriceExcludeInline(admin.TabularInline):
    model = PriceExclude
    extra = 0


class PriceInline(admin.TabularInline):
    model = Price
    extra = 0


class CruiseAdmin(admin.ModelAdmin):
    form = select2_modelform(Cruise, attrs={'width': '250px'})
    formfield_overrides = {models.ImageField: {'widget': image_widgets.AdminImagePreviewWidget}}
    prepopulated_fields = {'slug': ('title',)}
    fieldsets = (
        (None, {'fields': ('title', 'slug', 'subtitle', ('card_image', 'banner_image'), 'start_port', 'order_id')}),
        ('Offer text info', {'classes': ('collapse',),
                             'fields': ('cruise_description', 'cruise_program', 'afterword',)}),
        ('Yacht', {'classes': ('collapse',),
                             'fields': ('yacht', 'cabin', 'wc',)}),
        ('Price description', {'classes': ('collapse',),
                               'fields': ('price_description',)}),
    )
    inlines = [PriceInline, PriceIncludeInline, PriceExcludeInline, SinglePhotoInline]
    list_display = ('title', 'order_id')
    list_editable = ('order_id',)
    save_on_top = True

    def get_form(self, request, obj=None, **kwargs):
        if obj:
            self.formfield_overrides[models.TextField] = {'widget': RedactorEditor(redactor_options={'imageManagerJson': image_widgets.single_photo_json_url(obj.id, obj.photo_single.content_type.id),
                                                                                                     'buttonsHide': ['file']})}
        else:
            self.formfield_overrides[models.TextField] = {'widget': RedactorEditor()}
        return super(CruiseAdmin, self).get_form(request, obj, **kwargs)


admin.site.register(Theory, TheoryAdmin)
admin.site.register(TheoryStart)
admin.site.register(Cruise, CruiseAdmin)