from django.shortcuts import render, get_object_or_404
from django.views.generic import ListView, DetailView, TemplateView

from .models import Theory, TheoryStart, Cruise


class TheoryView(TemplateView):
    template_name = 'school/theory_detail.html'

    def get_context_data(self, **kwargs):
        context = super(TheoryView, self).get_context_data(**kwargs)
        context['theory'] = get_object_or_404(Theory, slug=kwargs['slug'])
        context['theory_start'] = TheoryStart.objects.all()
        context['theory_list'] = Theory.objects.all()
        return context


class CruiseList(ListView):
    queryset = Cruise.objects.prefetch_related('price_set').all()
    context_object_name = 'cruise_list'
    template_name = "school/cruise_list.html"


class CruiseDetail(DetailView):
    model = Cruise
    context_object_name = 'cruise'
    template_name = "school/cruise_detail.html"