# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('school', '0014_auto_20150917_2214'),
    ]

    operations = [
        migrations.AddField(
            model_name='theory',
            name='page_default',
            field=models.BooleanField(default=False),
        ),
    ]
