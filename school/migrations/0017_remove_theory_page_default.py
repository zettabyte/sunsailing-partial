# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('school', '0016_auto_20150926_0028'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='theory',
            name='page_default',
        ),
    ]
