# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('school', '0015_theory_page_default'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cruise',
            name='afterword',
            field=models.TextField(null=True, blank=True),
        ),
    ]
