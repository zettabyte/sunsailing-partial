# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('school', '0003_auto_20150704_2219'),
    ]

    operations = [
        migrations.RenameField(
            model_name='price',
            old_name='final_price',
            new_name='price',
        ),
    ]
