# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('school', '0002_delete_yacht'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='price',
            name='discount',
        ),
        migrations.RemoveField(
            model_name='price',
            name='initial_price',
        ),
    ]
