# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('school', '0011_delete_theorystarttable'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='theorystartafterword',
            name='theory',
        ),
        migrations.DeleteModel(
            name='TheoryStartAfterword',
        ),
    ]
