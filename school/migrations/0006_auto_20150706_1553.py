# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('school', '0005_auto_20150704_2321'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cruise',
            name='thumbnail_image',
            field=models.ImageField(upload_to='school/practicecruise/thumbnails/', help_text='Auto resize to 740px'),
        ),
    ]
