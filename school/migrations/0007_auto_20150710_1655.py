# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('school', '0006_auto_20150706_1553'),
    ]

    operations = [
        migrations.RenameField(
            model_name='cruise',
            old_name='header_image',
            new_name='banner_image',
        ),
        migrations.RenameField(
            model_name='cruise',
            old_name='thumbnail_image',
            new_name='card_image',
        ),
    ]
