# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('school', '0007_auto_20150710_1655'),
    ]

    operations = [
        migrations.AddField(
            model_name='cruise',
            name='subtitle',
            field=models.CharField(null=True, max_length=255),
        ),
    ]
