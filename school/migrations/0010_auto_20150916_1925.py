# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('school', '0009_auto_20150815_2229'),
    ]

    operations = [
        migrations.RenameField(
            model_name='theory',
            old_name='menu_title',
            new_name='sidebar_title',
        ),
    ]
