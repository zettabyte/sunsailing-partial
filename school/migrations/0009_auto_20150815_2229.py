# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('school', '0008_cruise_subtitle'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cruise',
            name='banner_image',
            field=models.ImageField(upload_to='school/cruise/banner_image/'),
        ),
        migrations.AlterField(
            model_name='cruise',
            name='card_image',
            field=models.ImageField(upload_to='school/cruise/card_image/', help_text='Auto resize to 740px'),
        ),
    ]
