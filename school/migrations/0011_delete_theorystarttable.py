# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('school', '0010_auto_20150916_1925'),
    ]

    operations = [
        migrations.DeleteModel(
            name='TheoryStartTable',
        ),
    ]
