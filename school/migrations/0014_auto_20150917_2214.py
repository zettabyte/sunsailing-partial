# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('school', '0013_auto_20150917_2202'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='theorystart',
            options={'ordering': ('start_date',)},
        ),
    ]
