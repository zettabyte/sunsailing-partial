# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('school', '0012_auto_20150917_2200'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='theorystart',
            name='start_info',
        ),
        migrations.RemoveField(
            model_name='theorystart',
            name='theory',
        ),
    ]
