# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('yachtcharter', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Cruise',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('slug', models.SlugField(unique=True)),
                ('header_image', models.ImageField(upload_to='school/practicecruise/headers/')),
                ('thumbnail_image', models.ImageField(help_text='Auto resize to 330px', upload_to='school/practicecruise/thumbnails/')),
                ('start_port', models.CharField(max_length=255)),
                ('cruise_description', models.TextField()),
                ('cruise_program', models.TextField()),
                ('afterword', models.TextField()),
                ('cabin', models.CharField(null=True, max_length=255)),
                ('wc', models.CharField(null=True, max_length=255)),
                ('price_description', models.TextField(null=True, blank=True)),
                ('order_id', models.IntegerField(null=True, blank=True)),
                ('yacht', models.ForeignKey(to='yachtcharter.Yacht', null=True)),
            ],
            options={
                'ordering': ('order_id',),
            },
        ),
        migrations.CreateModel(
            name='Price',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('date_from', models.DateField()),
                ('date_to', models.DateField()),
                ('initial_price', models.PositiveIntegerField(null=True, blank=True)),
                ('discount', models.PositiveIntegerField(null=True, blank=True)),
                ('final_price', models.PositiveIntegerField()),
                ('cruise', models.ForeignKey(to='school.Cruise')),
            ],
            options={
                'ordering': ('date_from',),
            },
        ),
        migrations.CreateModel(
            name='PriceExclude',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('exclude', models.CharField(max_length=255)),
                ('offer', models.ForeignKey(to='school.Cruise')),
            ],
        ),
        migrations.CreateModel(
            name='PriceInclude',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('include', models.CharField(max_length=255)),
                ('offer', models.ForeignKey(to='school.Cruise')),
            ],
        ),
        migrations.CreateModel(
            name='Theory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('menu_title', models.CharField(max_length=255)),
                ('slug', models.SlugField()),
                ('text', models.TextField()),
                ('order_id', models.IntegerField(null=True, blank=True)),
            ],
            options={
                'ordering': ('order_id',),
            },
        ),
        migrations.CreateModel(
            name='TheoryStart',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('start_date', models.DateField()),
                ('start_info', models.TextField()),
                ('theory', models.ForeignKey(to='school.Theory')),
            ],
        ),
        migrations.CreateModel(
            name='TheoryStartAfterword',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('text', models.TextField()),
                ('theory', models.OneToOneField(to='school.Theory')),
            ],
        ),
        migrations.CreateModel(
            name='TheoryStartTable',
            fields=[
            ],
            options={
                'proxy': True,
            },
            bases=('school.theory',),
        ),
        migrations.CreateModel(
            name='Yacht',
            fields=[
            ],
            options={
                'proxy': True,
            },
            bases=('school.cruise',),
        ),
    ]
