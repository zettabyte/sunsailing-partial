# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('school', '0004_auto_20150704_2220'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='priceexclude',
            options={'ordering': ('order_id',)},
        ),
        migrations.AlterModelOptions(
            name='priceinclude',
            options={'ordering': ('order_id',)},
        ),
        migrations.AddField(
            model_name='priceexclude',
            name='order_id',
            field=models.PositiveIntegerField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='priceinclude',
            name='order_id',
            field=models.PositiveIntegerField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='cruise',
            name='order_id',
            field=models.PositiveIntegerField(null=True, blank=True),
        ),
    ]
