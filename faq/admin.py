from django.contrib import admin

from redactor.widgets import RedactorEditor
from photobank.admin import SinglePhotoInline
from utils import image_widgets

from .models import *



class QuestionAdmin(admin.ModelAdmin):
    list_display = ('title', 'order_id')
    list_editable = ('order_id',)
    inlines = [SinglePhotoInline]
    save_on_top = True

    def get_form(self, request, obj=None, **kwargs):
        if obj:
            self.formfield_overrides[models.TextField] = {'widget': RedactorEditor(redactor_options={'imageManagerJson': image_widgets.single_photo_json_url(obj.id, obj.photo_single.content_type.id),
                                                                                                     'buttonsHide': ['file']})}
        else:
            self.formfield_overrides[models.TextField] = {'widget': RedactorEditor()}
        return super(QuestionAdmin, self).get_form(request, obj, **kwargs)



admin.site.register(Question, QuestionAdmin)