from django.views import generic

from .models import Question



class QuestionList(generic.ListView):
    model = Question
    context_object_name = 'question_list'
    template_name = "faq/question_list.html"