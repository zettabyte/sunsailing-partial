from django.db import models
from django.contrib.contenttypes.fields import GenericRelation



class Question(models.Model):
    title = models.CharField(max_length=255)
    answer = models.TextField()
    order_id = models.PositiveIntegerField()

    photo_single = GenericRelation('photobank.SinglePhoto')

    class Meta:
        ordering = ('order_id',)