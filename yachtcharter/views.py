from django.shortcuts import render, get_object_or_404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.exceptions import FieldError
from django.views.generic.base import TemplateView

from utils import data_utils

from .models import Region, YachtType, YachtCharter, CountryCharter, Country, Yacht


class CharterCatalogueView(TemplateView):
    template_name = 'yachtcharter/charter_catalogue.html'

    def get_context_data(self, **kwargs):
        context = super(CharterCatalogueView, self).get_context_data(**kwargs)
        context['charterregion_list'] = Region.objects.prefetch_related('country_set', 'country_set__countrycharter_set', 'country_set__countrycharter_set__yacht_type').all()
        context['yachttype_list'] = YachtType.objects.all()
        return context


class CharterYachtListView(TemplateView):
    template_name = 'yachtcharter/charter_yacht_list.html'

    def get_context_data(self, **kwargs):
        context = super(CharterYachtListView, self).get_context_data(**kwargs)
        context['country'] = get_object_or_404(Country, slug=kwargs['country_slug'])
        context['country_charter'] = get_object_or_404(CountryCharter, country__slug=kwargs['country_slug'], yacht_type__slug=kwargs['yacht_type_slug'])
        context['other_country_charter'] = CountryCharter.objects.filter(country__slug=kwargs['country_slug']).exclude(yacht_type__slug=kwargs['yacht_type_slug'])
        context['charter_yacht_list'] = YachtCharter.objects.filter(country__slug=kwargs['country_slug'], yacht__yacht_type__slug=kwargs['yacht_type_slug'])\
                                                    .prefetch_related('yacht__yacht_type',
                                                                      'yachtcharterpricelist_set__yachtchartercrewedpriceline_set',
                                                                      'yachtcharterpricelist_set__yachtcharterbareboatpriceline_set',
                                                                      'yachtcharterpriceextraoptions_set', 'yachtcharterpriceinclude_set', 'yachtcharterpriceexclude_set',
                                                                      'yacht__sailingyacht', 'yacht__catamaran', 'yacht__motoryacht', 'yacht__gulet', 'yacht__luxyacht')
        return context


class CatalogueYachtListView(TemplateView):
    template_name = 'yachtcharter/catalogue_yacht_list.html'

    def get_context_data(self, **kwargs):
        super(CatalogueYachtListView, self).get_context_data(**kwargs)
        yacht_type = get_object_or_404(YachtType, slug=kwargs['yacht_type_slug'])
        yacht_class = Yacht.get_class_by_type(yacht_type.yacht_type)
        yacht_list = yacht_class.objects.prefetch_related('shipyard', 'sailingyacht', 'catamaran', 'gulet', 'motoryacht', 'luxyacht', 'yacht_type').all()

        filter_params = self.request.GET.dict()
        active_filter = {}
        if 'country' in filter_params and filter_params['country'] != 'all':
            active_filter['country'] = filter_params['country']
            yacht_list = yacht_list.filter(yachtcharter__country__slug=filter_params['country'])

        if 'shipyard' in filter_params and filter_params['shipyard'] != 'all':
            active_filter['shipyard'] = filter_params['shipyard']
            yacht_list = yacht_list.filter(shipyard__slug=filter_params['shipyard'])

        if 'cabin' in filter_params and filter_params['cabin'] != 'all':
            active_filter['cabin'] = int(filter_params['cabin'])
            yacht_list = yacht_list.filter(cabin__contains=filter_params['cabin'])

        paginator = Paginator(yacht_list, 24)
        page = self.request.GET.get('page')
        try:
            yachts_on_page = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            yachts_on_page = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            yachts_on_page = paginator.page(paginator.num_pages)

        filter_list = {}

        cabin_list = Yacht.objects.filter(yacht_type__slug=kwargs['yacht_type_slug']).values_list('cabin', flat=True).distinct().order_by('cabin')

        filter_list['cabin'] = data_utils.cabin_flat_list(cabin_list)
        if yacht_type.yacht_type in (YachtType.GULET, YachtType.LUXYACHT):
            country_list = YachtCharter.objects.filter(yacht__yacht_type__slug=kwargs['yacht_type_slug']).values('country__title', 'country__slug').distinct().order_by('country__title')
            filter_list['country'] = country_list

        try:
            filter_list['shipyard'] = yacht_class.objects.filter(yacht_type__slug=kwargs['yacht_type_slug']).values('shipyard__slug', 'shipyard__title').distinct().order_by('shipyard__title')
        except FieldError:
            pass

        context = {'yacht_type': yacht_type,
                   'yacht_list': yachts_on_page,
                   'filter_list': filter_list,
                   'active_filter': active_filter
                   }
        return context


class YachtDetailView(TemplateView):
    template_name = 'yachtcharter/yacht_detail.html'

    def get_context_data(self, **kwargs):
        context = super(YachtDetailView, self).get_context_data(**kwargs)
        context['yacht_type'] = get_object_or_404(YachtType, slug=kwargs['yacht_type_slug'])
        context['yacht'] = get_object_or_404(Yacht.get_class_by_type(context.get('yacht_type').yacht_type).objects.prefetch_related('yachtcharter_set__yachtcharterpricelist_set__yachtchartercrewedpriceline_set',
                                                                                                                                    'yachtcharter_set__yachtcharterpricelist_set__yachtcharterbareboatpriceline_set',
                                                                                                                                    'yachtcharter_set__yachtcharterpriceextraoptions_set',
                                                                                                                                    'yachtcharter_set__yachtcharterpriceinclude_set',
                                                                                                                                    'yachtcharter_set__yachtcharterpriceexclude_set',
                                                                                                                                    'yachtcharter_set__country',
                                                                                                                                    'sailingyacht', 'catamaran', 'motoryacht', 'gulet', 'luxyacht'), slug=kwargs['yacht_slug'])
        return context