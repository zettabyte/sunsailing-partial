from django.conf.urls import url

from .views import *


urlpatterns = [
    url(r'^$', CharterCatalogueView.as_view(), name = 'main'),
    url(r'^(?P<yacht_type_slug>[\w-]+)-catalog/$', CatalogueYachtListView.as_view(), name='catalogue_yacht_list'),
    url(r'^(?P<yacht_type_slug>[\w-]+)-catalog/(?P<yacht_slug>[\w-]+)/$', YachtDetailView.as_view(), name='yacht_detail'),
    url(r'^(?P<country_slug>[\w-]+)/(?P<yacht_type_slug>[\w-]+)/$', CharterYachtListView.as_view(), name='charter_yacht_list'),
    ]
