# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Country',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('slug', models.SlugField(unique=True)),
                ('dropdown_menu_title', models.CharField(max_length=255)),
                ('image', models.ImageField(upload_to='yachtcharter/country/headers/')),
                ('thumbnail_image', models.ImageField(help_text='Auto resize to 736px', upload_to='yachtcharter/country/thumbnails/')),
                ('order_id', models.PositiveIntegerField(null=True, blank=True)),
            ],
            options={
                'ordering': ('order_id',),
            },
        ),
        migrations.CreateModel(
            name='CountryCharter',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('slug', models.SlugField()),
                ('country_text_top', models.TextField(null=True, blank=True)),
                ('country_text_bottom', models.TextField(null=True, blank=True)),
                ('order_id', models.PositiveIntegerField(null=True, blank=True)),
            ],
            options={
                'ordering': ('order_id',),
            },
        ),
        migrations.CreateModel(
            name='GuletClass',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('title', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='Region',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('order_id', models.PositiveIntegerField(null=True, blank=True)),
            ],
            options={
                'ordering': ('order_id',),
            },
        ),
        migrations.CreateModel(
            name='ShipYard',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('slug', models.SlugField(unique=True)),
            ],
            options={
                'ordering': ('title',),
            },
        ),
        migrations.CreateModel(
            name='Yacht',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('slug', models.SlugField(unique=True)),
                ('catalog_header_image', models.ImageField(help_text='Auto resize to 233px', upload_to='yachtcharter/yacht/')),
                ('length', models.DecimalField(decimal_places=2, max_digits=5)),
                ('beam', models.DecimalField(decimal_places=2, max_digits=4)),
                ('cabin', models.CommaSeparatedIntegerField(max_length=255)),
                ('bed', models.CharField(max_length=255)),
                ('wc', models.CommaSeparatedIntegerField(max_length=255)),
                ('brochure', models.FileField(null=True, upload_to='yachtcharter/brochure/', blank=True)),
            ],
            options={
                'ordering': ('length',),
            },
        ),
        migrations.CreateModel(
            name='YachtCharter',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('cabin', models.CharField(max_length=255)),
                ('wc', models.CharField(max_length=255)),
                ('country', models.ForeignKey(to='yachtcharter.Country')),
            ],
        ),
        migrations.CreateModel(
            name='YachtCharterBareboatPriceLine',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('yacht_year', models.CharField(max_length=4)),
                ('port', models.CharField(max_length=255)),
                ('date_from', models.DateField()),
                ('date_to', models.DateField()),
                ('initial_price', models.PositiveIntegerField(null=True, blank=True)),
                ('discount', models.PositiveIntegerField(null=True, blank=True)),
                ('final_price', models.PositiveIntegerField()),
            ],
            options={
                'ordering': ('date_from', 'final_price'),
            },
        ),
        migrations.CreateModel(
            name='YachtCharterCrewedPriceLine',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('date_from', models.DateField()),
                ('date_to', models.DateField()),
                ('week_price', models.PositiveIntegerField()),
            ],
            options={
                'ordering': ('date_from', 'week_price'),
            },
        ),
        migrations.CreateModel(
            name='YachtCharterPriceList',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('month_title', models.CharField(max_length=255)),
                ('order_id', models.PositiveIntegerField(null=True, blank=True)),
                ('yacht_charter', models.ForeignKey(to='yachtcharter.YachtCharter')),
            ],
            options={
                'ordering': ('order_id',),
            },
        ),
        migrations.CreateModel(
            name='YachtComfortPack',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('title', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='YachtEntertainment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('title', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='YachtType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('yacht_type', models.CharField(choices=[('sailingyacht', 'Sailing yachts'), ('catamaran', 'Catamarans'), ('motoryacht', 'Motor yachts'), ('gulet', 'Gulets'), ('luxyacht', 'Lux yachts')], max_length=255, unique=True)),
                ('title', models.CharField(max_length=255)),
                ('slug', models.SlugField()),
                ('title_nominative', models.CharField(max_length=255)),
                ('image', models.ImageField(upload_to='yachtcharter/yachttype/headers/')),
                ('thumbnail_image', models.ImageField(help_text='Auto resize to 736px', upload_to='yachtcharter/yachttype/thumbnails/')),
                ('media_image', models.ImageField(upload_to='yachtcharter/yachttype/country_charter_media_image/')),
            ],
        ),
        migrations.CreateModel(
            name='Catamaran',
            fields=[
                ('yacht_ptr', models.OneToOneField(to='yachtcharter.Yacht', auto_created=True, serialize=False, parent_link=True, primary_key=True)),
                ('engine_power', models.CharField(max_length=255)),
                ('fuel_tank', models.CharField(max_length=255)),
                ('water_tank', models.CharField(max_length=255)),
                ('draft', models.DecimalField(decimal_places=2, max_digits=5)),
                ('comfort_pack', models.ManyToManyField(to='yachtcharter.YachtComfortPack', blank=True)),
                ('entertainment', models.ManyToManyField(to='yachtcharter.YachtEntertainment', blank=True)),
                ('shipyard', models.ForeignKey(to='yachtcharter.ShipYard')),
            ],
            options={
                'abstract': False,
            },
            bases=('yachtcharter.yacht',),
        ),
        migrations.CreateModel(
            name='Gulet',
            fields=[
                ('yacht_ptr', models.OneToOneField(to='yachtcharter.Yacht', auto_created=True, serialize=False, parent_link=True, primary_key=True)),
                ('built_year', models.CharField(max_length=255)),
                ('crew_num', models.CharField(max_length=255)),
                ('crew_info', models.TextField()),
                ('cabin_info', models.TextField()),
                ('comfort_pack', models.ManyToManyField(to='yachtcharter.YachtComfortPack', blank=True)),
                ('entertainment', models.ManyToManyField(to='yachtcharter.YachtEntertainment', blank=True)),
                ('gulet_class', models.ForeignKey(to='yachtcharter.GuletClass')),
            ],
            options={
                'abstract': False,
            },
            bases=('yachtcharter.yacht',),
        ),
        migrations.CreateModel(
            name='LuxYacht',
            fields=[
                ('yacht_ptr', models.OneToOneField(to='yachtcharter.Yacht', auto_created=True, serialize=False, parent_link=True, primary_key=True)),
                ('built_year', models.CharField(max_length=255)),
                ('crew_num', models.CharField(max_length=255)),
                ('crew_info', models.TextField()),
                ('cabin_info', models.TextField()),
                ('engine_power', models.CharField(max_length=255)),
                ('max_speed', models.CharField(max_length=255)),
                ('cruising_speed', models.CharField(max_length=255)),
                ('fuel_consumption', models.CharField(max_length=255)),
                ('comfort_pack', models.ManyToManyField(to='yachtcharter.YachtComfortPack', blank=True)),
                ('entertainment', models.ManyToManyField(to='yachtcharter.YachtEntertainment', blank=True)),
                ('shipyard', models.ForeignKey(to='yachtcharter.ShipYard')),
            ],
            options={
                'abstract': False,
            },
            bases=('yachtcharter.yacht',),
        ),
        migrations.CreateModel(
            name='MotorYacht',
            fields=[
                ('yacht_ptr', models.OneToOneField(to='yachtcharter.Yacht', auto_created=True, serialize=False, parent_link=True, primary_key=True)),
                ('engine_power', models.CharField(max_length=255)),
                ('fuel_tank', models.CharField(max_length=255)),
                ('water_tank', models.CharField(max_length=255)),
                ('draft', models.DecimalField(decimal_places=2, max_digits=5)),
                ('max_speed', models.CharField(max_length=255)),
                ('cruising_speed', models.CharField(max_length=255)),
                ('fuel_consumption', models.CharField(max_length=255)),
                ('cabin_info', models.TextField()),
                ('shipyard', models.ForeignKey(to='yachtcharter.ShipYard')),
            ],
            options={
                'abstract': False,
            },
            bases=('yachtcharter.yacht',),
        ),
        migrations.CreateModel(
            name='SailingYacht',
            fields=[
                ('yacht_ptr', models.OneToOneField(to='yachtcharter.Yacht', auto_created=True, serialize=False, parent_link=True, primary_key=True)),
                ('engine_power', models.CharField(max_length=255)),
                ('fuel_tank', models.CharField(max_length=255)),
                ('water_tank', models.CharField(max_length=255)),
                ('draft', models.DecimalField(decimal_places=2, max_digits=5)),
                ('shipyard', models.ForeignKey(to='yachtcharter.ShipYard')),
            ],
            options={
                'abstract': False,
            },
            bases=('yachtcharter.yacht',),
        ),
        migrations.AddField(
            model_name='yachtchartercrewedpriceline',
            name='price_list',
            field=models.ForeignKey(to='yachtcharter.YachtCharterPriceList'),
        ),
        migrations.AddField(
            model_name='yachtcharterbareboatpriceline',
            name='price_list',
            field=models.ForeignKey(to='yachtcharter.YachtCharterPriceList'),
        ),
        migrations.AddField(
            model_name='yachtcharter',
            name='yacht',
            field=models.ForeignKey(to='yachtcharter.Yacht'),
        ),
        migrations.AddField(
            model_name='yacht',
            name='yacht_type',
            field=models.ForeignKey(to='yachtcharter.YachtType'),
        ),
        migrations.AddField(
            model_name='countrycharter',
            name='charter_yacht_type',
            field=models.ForeignKey(to='yachtcharter.YachtType'),
        ),
        migrations.AddField(
            model_name='countrycharter',
            name='country',
            field=models.ForeignKey(to='yachtcharter.Country'),
        ),
        migrations.AddField(
            model_name='country',
            name='region',
            field=models.ForeignKey(to='yachtcharter.Region'),
        ),
        migrations.CreateModel(
            name='YachtCharterBareboatPriceList',
            fields=[
            ],
            options={
                'proxy': True,
            },
            bases=('yachtcharter.yachtcharterpricelist',),
        ),
        migrations.CreateModel(
            name='YachtCharterCrewedPriceList',
            fields=[
            ],
            options={
                'proxy': True,
            },
            bases=('yachtcharter.yachtcharterpricelist',),
        ),
        migrations.AlterUniqueTogether(
            name='yachtcharter',
            unique_together=set([('country', 'yacht')]),
        ),
        migrations.AlterUniqueTogether(
            name='countrycharter',
            unique_together=set([('country', 'charter_yacht_type'), ('country', 'slug')]),
        ),
    ]
