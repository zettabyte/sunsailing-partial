# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('yachtcharter', '0024_auto_20150803_1408'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='sailingyacht',
            options={'ordering': ('length', 'title')},
        ),
        migrations.AlterModelOptions(
            name='yacht',
            options={'ordering': ('length', 'title')},
        ),
    ]
