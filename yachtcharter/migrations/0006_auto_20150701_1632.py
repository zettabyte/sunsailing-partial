# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('yachtcharter', '0005_auto_20150701_1625'),
    ]

    operations = [
        migrations.RenameField(
            model_name='countrycharter',
            old_name='country_text_bottom',
            new_name='text_bottom',
        ),
        migrations.RenameField(
            model_name='countrycharter',
            old_name='country_text_top',
            new_name='text_top',
        ),
        migrations.RenameField(
            model_name='countrycharter',
            old_name='charter_yacht_type',
            new_name='yacht_type',
        ),
        migrations.AlterField(
            model_name='countrycharter',
            name='country',
            field=models.ForeignKey(verbose_name='Title', to='yachtcharter.Country'),
        ),
        migrations.AlterUniqueTogether(
            name='countrycharter',
            unique_together=set([('country', 'sub_slug'), ('country', 'yacht_type')]),
        ),
    ]
