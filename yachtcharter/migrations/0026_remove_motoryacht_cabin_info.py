# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('yachtcharter', '0025_auto_20150803_1410'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='motoryacht',
            name='cabin_info',
        ),
    ]
