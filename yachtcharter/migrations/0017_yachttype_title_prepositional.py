# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('yachtcharter', '0016_auto_20150704_1447'),
    ]

    operations = [
        migrations.AddField(
            model_name='yachttype',
            name='title_prepositional',
            field=models.CharField(null=True, max_length=255),
        ),
    ]
