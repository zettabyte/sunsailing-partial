# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('yachtcharter', '0028_auto_20150815_2258'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='yachtcomfortpack',
            options={'ordering': ('order_id',)},
        ),
        migrations.AlterModelOptions(
            name='yachtentertainment',
            options={'ordering': ('order_id',)},
        ),
        migrations.AddField(
            model_name='yachtcomfortpack',
            name='order_id',
            field=models.PositiveIntegerField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='yachtentertainment',
            name='order_id',
            field=models.PositiveIntegerField(null=True, blank=True),
        ),
    ]
