# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('yachtcharter', '0013_yachttype_title_genitive'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='yachttype',
            options={'ordering': ('order_id',)},
        ),
        migrations.AddField(
            model_name='yachttype',
            name='order_id',
            field=models.PositiveIntegerField(blank=True, null=True),
        ),
    ]
