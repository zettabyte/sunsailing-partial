# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('yachtcharter', '0026_remove_motoryacht_cabin_info'),
    ]

    operations = [
        migrations.CreateModel(
            name='YachtCharterPriceExclude',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('exclude', models.CharField(max_length=255)),
                ('order_id', models.PositiveIntegerField(null=True, blank=True)),
                ('yacht_charter', models.ForeignKey(to='yachtcharter.YachtCharter')),
            ],
            options={
                'ordering': ('order_id',),
            },
        ),
        migrations.CreateModel(
            name='YachtCharterPriceExtraOptions',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=255)),
                ('description', models.TextField()),
                ('order_id', models.PositiveIntegerField(null=True, blank=True)),
                ('yacht_charter', models.ForeignKey(to='yachtcharter.YachtCharter')),
            ],
            options={
                'ordering': ('order_id',),
            },
        ),
        migrations.CreateModel(
            name='YachtCharterPriceInclude',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('include', models.CharField(max_length=255)),
                ('order_id', models.PositiveIntegerField(null=True, blank=True)),
                ('yacht_charter', models.ForeignKey(to='yachtcharter.YachtCharter')),
            ],
            options={
                'ordering': ('order_id',),
            },
        ),
    ]
