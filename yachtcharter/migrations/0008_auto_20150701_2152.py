# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('yachtcharter', '0007_auto_20150701_1645'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='gulet',
            name='built_year',
        ),
        migrations.RemoveField(
            model_name='luxyacht',
            name='built_year',
        ),
        migrations.AddField(
            model_name='yacht',
            name='built_year',
            field=models.CharField(max_length=255, null=True),
        ),
    ]
