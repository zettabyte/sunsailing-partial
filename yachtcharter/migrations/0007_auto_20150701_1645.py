# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('yachtcharter', '0006_auto_20150701_1632'),
    ]

    operations = [
        migrations.AddField(
            model_name='yachttype',
            name='title_charter',
            field=models.CharField(help_text='This is title like "Sailing yacht charter"', max_length=255, null=True),
        ),
        migrations.AlterUniqueTogether(
            name='countrycharter',
            unique_together=set([('country', 'yacht_type')]),
        ),
        migrations.RemoveField(
            model_name='countrycharter',
            name='sub_slug',
        ),
        migrations.RemoveField(
            model_name='countrycharter',
            name='sub_title',
        ),
    ]
