# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('yachtcharter', '0008_auto_20150701_2152'),
    ]

    operations = [
        migrations.AddField(
            model_name='yachtcharter',
            name='built_year',
            field=models.CharField(null=True, max_length=255),
        ),
    ]
