# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('yachtcharter', '0015_auto_20150703_2141'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='yachtcharter',
            options={'ordering': ('yacht__length', 'country__order_id')},
        ),
    ]
