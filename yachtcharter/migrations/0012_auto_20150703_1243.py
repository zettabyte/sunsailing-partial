# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('yachtcharter', '0011_auto_20150702_1716'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='yachtcharter',
            options={'ordering': ('yacht__length',)},
        ),
        migrations.AlterField(
            model_name='yacht',
            name='built_year',
            field=models.CharField(max_length=255, help_text='Years of when model was produced', blank=True, null=True),
        ),
    ]
