# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('yachtcharter', '0002_auto_20150701_1547'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='country',
            name='sub_title',
        ),
    ]
