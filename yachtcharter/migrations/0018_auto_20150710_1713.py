# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('yachtcharter', '0017_yachttype_title_prepositional'),
    ]

    operations = [
        migrations.RenameField(
            model_name='yachttype',
            old_name='image',
            new_name='banner_image',
        ),
        migrations.RenameField(
            model_name='yachttype',
            old_name='thumbnail_image',
            new_name='card_image',
        ),
        migrations.AddField(
            model_name='yachttype',
            name='subtitle',
            field=models.CharField(null=True, max_length=255),
        ),
    ]
