# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('yachtcharter', '0012_auto_20150703_1243'),
    ]

    operations = [
        migrations.AddField(
            model_name='yachttype',
            name='title_genitive',
            field=models.CharField(null=True, max_length=255),
        ),
    ]
