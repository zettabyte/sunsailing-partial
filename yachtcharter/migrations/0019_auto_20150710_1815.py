# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('yachtcharter', '0018_auto_20150710_1713'),
    ]

    operations = [
        migrations.RenameField(
            model_name='country',
            old_name='image',
            new_name='banner_image',
        ),
        migrations.RenameField(
            model_name='country',
            old_name='thumbnail_image',
            new_name='card_image',
        ),
    ]
