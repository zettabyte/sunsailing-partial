# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('yachtcharter', '0022_auto_20150803_1404'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='catamaran',
            options={'ordering': ('length', 'title')},
        ),
        migrations.AlterModelOptions(
            name='gulet',
            options={'ordering': ('length', 'title')},
        ),
        migrations.AlterModelOptions(
            name='luxyacht',
            options={'ordering': ('length', 'title')},
        ),
        migrations.AlterModelOptions(
            name='motoryacht',
            options={'ordering': ('length', 'title')},
        ),
        migrations.AlterModelOptions(
            name='sailingyacht',
            options={'ordering': ('length', 'title')},
        ),
        migrations.AlterModelOptions(
            name='yacht',
            options={'ordering': ('length', 'title')},
        ),
    ]
