# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('yachtcharter', '0010_remove_yachtcharterbareboatpriceline_yacht_year'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='yachtcharterbareboatpriceline',
            name='port',
        ),
        migrations.AddField(
            model_name='yachtcharter',
            name='start_port',
            field=models.CharField(max_length=255, null=True),
        ),
    ]
