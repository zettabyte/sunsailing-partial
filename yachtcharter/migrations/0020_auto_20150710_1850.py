# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('yachtcharter', '0019_auto_20150710_1815'),
    ]

    operations = [
        migrations.RenameField(
            model_name='yacht',
            old_name='catalog_header_image',
            new_name='card_image',
        ),
    ]
