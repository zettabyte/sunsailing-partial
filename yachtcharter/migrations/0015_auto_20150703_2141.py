# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('yachtcharter', '0014_auto_20150703_1406'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='catamaran',
            options={'ordering': ('length',)},
        ),
        migrations.AlterModelOptions(
            name='gulet',
            options={'ordering': ('length',)},
        ),
        migrations.AlterModelOptions(
            name='luxyacht',
            options={'ordering': ('length',)},
        ),
        migrations.AlterModelOptions(
            name='motoryacht',
            options={'ordering': ('length',)},
        ),
        migrations.AlterModelOptions(
            name='sailingyacht',
            options={'ordering': ('length',)},
        ),
        migrations.AlterField(
            model_name='yacht',
            name='catalog_header_image',
            field=models.ImageField(help_text='Auto resize to 740px', upload_to='yachtcharter/yacht/'),
        ),
        migrations.AlterField(
            model_name='yachttype',
            name='thumbnail_image',
            field=models.ImageField(help_text='Auto resize to 740px', upload_to='yachtcharter/yachttype/thumbnails/'),
        ),
    ]
