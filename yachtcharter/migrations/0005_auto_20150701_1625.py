# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('yachtcharter', '0004_country_sub_title'),
    ]

    operations = [
        migrations.RenameField(
            model_name='countrycharter',
            old_name='slug',
            new_name='sub_slug',
        ),
        migrations.RenameField(
            model_name='countrycharter',
            old_name='title',
            new_name='sub_title',
        ),
        migrations.AlterUniqueTogether(
            name='countrycharter',
            unique_together=set([('country', 'charter_yacht_type'), ('country', 'sub_slug')]),
        ),
    ]
