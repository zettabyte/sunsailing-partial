# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('yachtcharter', '0009_yachtcharter_built_year'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='yachtcharterbareboatpriceline',
            name='yacht_year',
        ),
    ]
