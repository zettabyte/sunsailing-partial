# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('yachtcharter', '0027_yachtcharterpriceexclude_yachtcharterpriceextraoptions_yachtcharterpriceinclude'),
    ]

    operations = [
        migrations.AlterField(
            model_name='country',
            name='banner_image',
            field=models.ImageField(upload_to='yachtcharter/country/banner_image/'),
        ),
        migrations.AlterField(
            model_name='country',
            name='card_image',
            field=models.ImageField(help_text='Auto resize to 736px', upload_to='yachtcharter/country/card_image/'),
        ),
        migrations.AlterField(
            model_name='yachttype',
            name='banner_image',
            field=models.ImageField(upload_to='yachtcharter/yachttype/banner_image/'),
        ),
        migrations.AlterField(
            model_name='yachttype',
            name='card_image',
            field=models.ImageField(help_text='Auto resize to 740px', upload_to='yachtcharter/yachttype/card_image/'),
        ),
    ]
