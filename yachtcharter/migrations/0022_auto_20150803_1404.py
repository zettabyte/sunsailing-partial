# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('yachtcharter', '0021_auto_20150803_1331'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='yacht',
            options={'ordering': ('length', '-title')},
        ),
    ]
