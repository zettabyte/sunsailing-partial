# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('yachtcharter', '0029_auto_20150819_0140'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='yachttype',
            name='subtitle',
        ),
        migrations.AlterField(
            model_name='yachttype',
            name='title_charter',
            field=models.CharField(help_text='Something like "Sailing yacht charter"', max_length=255, null=True),
        ),
    ]
