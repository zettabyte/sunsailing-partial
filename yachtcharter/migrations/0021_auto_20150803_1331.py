# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('yachtcharter', '0020_auto_20150710_1850'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='yacht',
            options={'ordering': ('length', 'title')},
        ),
    ]
