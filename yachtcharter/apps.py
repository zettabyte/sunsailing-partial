from django.apps import AppConfig


class YachtCharterConfig(AppConfig):
    name = 'yachtcharter'
    verbose_name = 'Yacht Charter'

    def ready(self):
        import yachtcharter.signals