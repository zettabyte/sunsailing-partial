from django.db.models.signals import pre_save, pre_delete
from django.dispatch import receiver

from utils import signal_utils

from .models import LuxYacht, Gulet, MotorYacht, Catamaran, SailingYacht, YachtType, Country


@receiver(pre_save, sender=LuxYacht)
@receiver(pre_save, sender=Gulet)
@receiver(pre_save, sender=MotorYacht)
@receiver(pre_save, sender=Catamaran)
@receiver(pre_save, sender=SailingYacht)
@receiver(pre_save, sender=YachtType)
@receiver(pre_save, sender=Country)
def del_exist_img(instance, sender, **kwargs):
    signal_utils.del_file_on_replace_item(instance, sender)


@receiver(pre_delete, sender=LuxYacht)
@receiver(pre_delete, sender=Gulet)
@receiver(pre_delete, sender=MotorYacht)
@receiver(pre_delete, sender=Catamaran)
@receiver(pre_delete, sender=SailingYacht)
@receiver(pre_delete, sender=YachtType)
@receiver(pre_delete, sender=Country)
def del_item(instance, **kwargs):
    signal_utils.del_file_on_del_item(instance)