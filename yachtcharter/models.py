import os

from django.db import models
from django.core.urlresolvers import reverse
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericRelation

from utils import img_utils


class Region(models.Model):
    title = models.CharField(max_length=255)
    order_id = models.PositiveIntegerField(blank=True, null=True)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ('order_id',)


class Country(models.Model):
    title = models.CharField(max_length=255)
    slug = models.SlugField(unique=True)
    sub_title = models.CharField(max_length=255, null=True)
    banner_image = models.ImageField(upload_to='yachtcharter/country/banner_image/')
    card_image = models.ImageField(upload_to='yachtcharter/country/card_image/', help_text='Auto resize to 736px')
    region = models.ForeignKey(Region)
    order_id = models.PositiveIntegerField(blank=True, null=True)

    def save(self, *args, **kwargs):
        if not os.path.isfile(self.card_image.path):
            self.card_image = img_utils.resize_ratio(736, self.card_image.file)
        super(Country, self).save(*args, **kwargs)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ('order_id',)


class YachtType(models.Model):
    # Set of classes names which used in ContentType
    SAILINGYACHT = 'sailingyacht'
    CATAMARAN = 'catamaran'
    MOTORYACHT = 'motoryacht'
    GULET = 'gulet'
    LUXYACHT = 'luxyacht'

    YACHT_TYPE = (
        (SAILINGYACHT, 'Sailing yachts'),
        (CATAMARAN, 'Catamarans'),
        (MOTORYACHT, 'Motor yachts'),
        (GULET, 'Gulets'),
        (LUXYACHT, 'Lux yachts')
    )
    yacht_type = models.CharField(max_length=255, choices=YACHT_TYPE, unique=True)

    title = models.CharField(max_length=255)
    slug = models.SlugField()
    title_nominative = models.CharField(max_length=255)
    title_genitive = models.CharField(max_length=255, null=True)
    title_prepositional = models.CharField(max_length=255, null=True)
    title_charter = models.CharField(max_length=255, null=True, help_text='Something like "Sailing yacht charter"')

    banner_image = models.ImageField(upload_to='yachtcharter/yachttype/banner_image/')
    card_image = models.ImageField(upload_to='yachtcharter/yachttype/card_image/', help_text='Auto resize to 740px')
    media_image = models.ImageField(upload_to='yachtcharter/yachttype/country_charter_media_image/')

    order_id = models.PositiveIntegerField(blank=True, null=True)

    def save(self, *args, **kwargs):
        if not os.path.isfile(self.card_image.path):
            img = self.card_image.file
            self.card_image = img_utils.resize_ratio(740, img)
            self.media_image = img_utils.resize_ratio(120, img)
        super(YachtType, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('yachtcharter:catalogue_yacht_list', args=[self.slug])

    def __str__(self):
        return self.title

    class Meta:
        ordering = ('order_id',)


class CountryCharter(models.Model):
    country = models.ForeignKey(Country,  verbose_name='Title')
    yacht_type = models.ForeignKey(YachtType)
    text_top = models.TextField(null=True, blank=True)
    text_bottom = models.TextField(null=True, blank=True)
    order_id = models.PositiveIntegerField(blank=True, null=True)

    def __str__(self):
        return self.yacht_type.title_charter + ' | ' + self.country.title

    def get_absolute_url(self):
        return reverse('yachtcharter:charter_yacht_list', args=[self.country.slug, self.yacht_type.slug])

    class Meta:
        unique_together = (('country', 'yacht_type'))
        ordering = ('order_id',)


class ShipYard(models.Model):
    title = models.CharField(max_length=255)
    slug = models.SlugField(unique=True)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ('title',)


class YachtEntertainment(models.Model):
    title = models.CharField(max_length=255)
    order_id = models.PositiveIntegerField(blank=True, null=True)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ('order_id',)


class YachtComfortPack(models.Model):
    title = models.CharField(max_length=255)
    order_id = models.PositiveIntegerField(blank=True, null=True)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ('order_id',)


class Yacht(models.Model):
    title = models.CharField(max_length=255)
    slug = models.SlugField(unique=True)
    card_image = models.ImageField(upload_to='yachtcharter/yacht/', help_text = 'Auto resize to 740px')
    built_year = models.CharField(max_length=255, null=True, blank=True, help_text='Years of when model was produced')
    yacht_type = models.ForeignKey(YachtType)
    length = models.DecimalField(max_digits=5, decimal_places=2)
    beam = models.DecimalField(max_digits=4, decimal_places=2)
    cabin = models.CommaSeparatedIntegerField(max_length=255)
    bed = models.CharField(max_length=255)
    wc = models.CommaSeparatedIntegerField(max_length=255)
    brochure = models.FileField(upload_to='yachtcharter/brochure/', null=True, blank=True)
    photo_gallery = GenericRelation('photobank.YachtPhotoGallery')

    @staticmethod
    def get_class_by_type(yacht_type):
        return ContentType.objects.get(app_label="yachtcharter", model=yacht_type).model_class()

    def get_obj_by_type(self):
        return ContentType.objects.get(app_label="yachtcharter", model=self.yacht_type.yacht_type).get_object_for_this_type(yacht_ptr_id = self.id)

    def __str__(self):
        res = self.title + ' | ' + self.cabin + ' cabins'
        if self.built_year:
            res += ' | ' + self.built_year + ' year'
        return  res

    def _comma_separated_display(self, value):
        return value.replace(',', ' / ')

    @property
    def cabin_display(self):
        return self._comma_separated_display(self.cabin)

    @property
    def wc_display(self):
        return self._comma_separated_display(self.wc)

    def _to_ft(self, meter):
        return round((float(meter) * 3.28), 2)

    @property
    def length_ft(self):
        return self._to_ft(self.length)

    @property
    def beam_ft(self):
        return self._to_ft(self.beam)

    def save(self, *args, **kwargs):
        if not os.path.isfile(self.card_image.path):
            self.card_image = img_utils.resize_ratio(740, self.card_image.file)
        super(Yacht, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('yachtcharter:yacht_detail', args=[self.yacht_type.slug, self.slug])

    class Meta:
        ordering = ('length', 'title')


class BareboatYacht(Yacht):
    engine_power = models.CharField(max_length=255)
    fuel_tank = models.CharField(max_length=255)
    water_tank = models.CharField(max_length=255)
    shipyard = models.ForeignKey(ShipYard)
    draft = models.DecimalField(max_digits=5, decimal_places=2)

    def draft_ft(self):
        return self._to_ft(self.draft)

    class Meta:
        abstract = True


class SailingYacht(BareboatYacht):
    def save(self, *args, **kwargs):
        self.yacht_type = YachtType.objects.get(yacht_type=YachtType.SAILINGYACHT)
        super(SailingYacht, self).save(*args, **kwargs)

    class Meta:
        ordering = ('length', 'title')


class Catamaran(BareboatYacht):
    entertainment = models.ManyToManyField(YachtEntertainment, blank=True)
    comfort_pack = models.ManyToManyField(YachtComfortPack, blank=True)
    def save(self, *args, **kwargs):
        self.yacht_type = YachtType.objects.get(yacht_type=YachtType.CATAMARAN)
        super(Catamaran, self).save(*args, **kwargs)

    class Meta:
        ordering = ('length', 'title')


class MotorYacht(BareboatYacht):
    max_speed = models.CharField(max_length=255)
    cruising_speed = models.CharField(max_length=255)
    fuel_consumption = models.CharField(max_length=255)

    def save(self, *args, **kwargs):
        self.yacht_type = YachtType.objects.get(yacht_type=YachtType.MOTORYACHT)
        super(MotorYacht, self).save(*args, **kwargs)

    class Meta:
        ordering = ('length', 'title')


class CrewedYacht(Yacht):
    crew_num = models.CharField(max_length=255)
    crew_info = models.TextField()
    cabin_info = models.TextField()
    entertainment = models.ManyToManyField(YachtEntertainment, blank=True)
    comfort_pack = models.ManyToManyField(YachtComfortPack, blank=True)

    class Meta:
        abstract = True


class GuletClass(models.Model):
    title = models.CharField(max_length=255)

    def __str__(self):
        return self.title


class Gulet(CrewedYacht):
    gulet_class = models.ForeignKey(GuletClass)

    def save(self, *args, **kwargs):
        self.yacht_type = YachtType.objects.get(yacht_type=YachtType.GULET)
        super(Gulet, self).save(*args, **kwargs)

    class Meta:
        ordering = ('length', 'title')


class LuxYacht(CrewedYacht):
    shipyard = models.ForeignKey(ShipYard)
    engine_power = models.CharField(max_length=255)
    max_speed = models.CharField(max_length=255)
    cruising_speed = models.CharField(max_length=255)
    fuel_consumption = models.CharField(max_length=255)

    def save(self, *args, **kwargs):
        self.yacht_type = YachtType.objects.get(yacht_type=YachtType.LUXYACHT)
        super(LuxYacht, self).save(*args, **kwargs)

    class Meta:
        ordering = ('length', 'title')


class YachtCharter(models.Model):
    country = models.ForeignKey(Country)
    yacht = models.ForeignKey(Yacht)
    built_year = models.CharField(max_length=255, null=True)
    cabin = models.CharField(max_length=255)
    wc = models.CharField(max_length=255)
    start_port = models.CharField(max_length=255, null=True)

    def crewed_charter_price_from(self):
        return YachtCharter.objects.annotate(value=models.Min('yachtcharterpricelist__yachtchartercrewedpriceline__week_price')).get(id=self.id)

    def __str__(self):
        return ' | '.join([self.yacht.title, self.country.title])

    class Meta:
        ordering = ('yacht__length', 'country__order_id')
        unique_together = ('country', 'yacht')

class YachtCharterPriceInclude(models.Model):
    yacht_charter = models.ForeignKey(YachtCharter)
    include = models.CharField(max_length=255)
    order_id = models.PositiveIntegerField(blank=True, null=True)

    class Meta:
        ordering = ('order_id',)


class YachtCharterPriceExclude(models.Model):
    yacht_charter = models.ForeignKey(YachtCharter)
    exclude = models.CharField(max_length=255)
    order_id = models.PositiveIntegerField(blank=True, null=True)

    class Meta:
        ordering = ('order_id',)


class YachtCharterPriceExtraOptions(models.Model):
    yacht_charter = models.ForeignKey(YachtCharter)
    title = models.CharField(max_length=255)
    description = models.TextField()
    order_id = models.PositiveIntegerField(blank=True, null=True)

    class Meta:
        ordering = ('order_id',)


class YachtCharterPriceList(models.Model):
    month_title = models.CharField(max_length=255)
    yacht_charter = models.ForeignKey(YachtCharter)
    order_id = models.PositiveIntegerField(blank=True, null=True)

    def __str__(self):
        return self.month_title + ' | ' + self.yacht_charter.__str__()

    class Meta:
        ordering = ('order_id',)


class YachtCharterBareboatManager(models.Manager):
    def get_queryset(self):
        return super(YachtCharterBareboatManager, self).get_queryset().filter(yachtcharterbareboatpriceline__isnull = False).distinct()


class YachtCharterBareboatPriceList(YachtCharterPriceList):
    objects = YachtCharterBareboatManager()
    class Meta:
        proxy = True


class YachtCharterCrewedManager(models.Manager):
    def get_queryset(self):
        return super(YachtCharterCrewedManager, self).get_queryset().filter(yachtchartercrewedpriceline__isnull = False).distinct()


class YachtCharterCrewedPriceList(YachtCharterPriceList):
    objects = YachtCharterCrewedManager()
    class Meta:
        proxy = True


class YachtCharterBareboatPriceLine(models.Model):
    price_list = models.ForeignKey(YachtCharterPriceList)
    date_from = models.DateField()
    date_to = models.DateField()
    initial_price = models.PositiveIntegerField(blank=True, null=True)
    discount = models.PositiveIntegerField(blank=True, null=True)
    final_price = models.PositiveIntegerField()

    class Meta:
        ordering = ('date_from', 'final_price')


class YachtCharterCrewedPriceLine(models.Model):
    price_list = models.ForeignKey(YachtCharterPriceList)
    date_from = models.DateField()
    date_to = models.DateField()
    week_price = models.PositiveIntegerField()

    @property
    def day_price(self):
        return round((self.week_price / 7))

    class Meta:
        ordering = ('date_from', 'week_price')
