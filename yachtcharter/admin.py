from django.contrib import admin

from easy_select2 import select2_modelform
from redactor.widgets import RedactorEditor
from photobank.admin import YachtPhotoGalleryInline
from utils import data_utils, image_widgets

from .models import *


class RegionAdmin(admin.ModelAdmin):
    list_display = ('title', 'order_id')
    list_editable = ('order_id',)
    save_on_top = True


class CountryAdmin(admin.ModelAdmin):
    list_display = ('title', 'order_id')
    list_editable = ('order_id',)
    prepopulated_fields = {'slug': ('title',)}
    list_filter = ('region',)
    save_on_top = True
    formfield_overrides = {models.ImageField: {'widget': image_widgets.AdminImagePreviewWidget}}


class CountryCharterAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'order_id')
    list_editable = ('order_id',)
    form = select2_modelform(CountryCharter, attrs={'width': '250px'})
    formfield_overrides = {models.TextField: {'widget': RedactorEditor()}}
    list_filter = ('country',)
    save_on_top = True


class YachtCharterPriceIncludeInline(admin.TabularInline):
    model = YachtCharterPriceInclude
    extra = 0


class YachtCharterPriceExcludeInline(admin.TabularInline):
    model = YachtCharterPriceExclude
    extra = 0


class YachtCharterPriceExtraOptions(admin.TabularInline):
    model = YachtCharterPriceExtraOptions
    formfield_overrides = {models.TextField: {'widget': RedactorEditor()}}
    extra = 0


class YachtCharterAdmin(admin.ModelAdmin):
    form = select2_modelform(YachtCharter, attrs={'width': '350px'})
    list_filter = ('country', 'yacht__yacht_type')
    inlines = [YachtCharterPriceIncludeInline, YachtCharterPriceExcludeInline, YachtCharterPriceExtraOptions]
    save_on_top = True
    save_as = True


class YachtTypeAdmin(admin.ModelAdmin):
    form = select2_modelform(YachtType, attrs={'width': '250px'})
    exclude = ('media_image',)
    prepopulated_fields = {'slug': ('title',)}
    save_on_top = True
    formfield_overrides = {models.ImageField: {'widget': image_widgets.AdminImagePreviewWidget}}
    list_display = ('title', 'order_id')
    list_editable = ('order_id',)


class ShipYardAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}
    save_on_top = True


class CabinFilter(admin.SimpleListFilter):
    title = ('yacht cabins')
    parameter_name = 'cabin'

    def lookups(self, request, model_admin):
        cabin_list = model_admin.model.objects.values_list('cabin', flat=True).distinct().order_by('cabin')
        flat_list = data_utils.cabin_flat_list(cabin_list)
        return list(zip(flat_list, [str(i) + ' cabins' for i in flat_list]))

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(cabin__contains=self.value())


class SailingYachtAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title', 'cabin', 'built_year')}
    fieldsets = (
        (None, {'fields': ('title', 'slug', 'built_year', 'shipyard', 'card_image')}),
        ('Accommodation', {'fields': ('cabin', 'bed', 'wc')}),
        ('Technical parameters', {'fields': ('length', 'beam', 'draft', 'engine_power', 'fuel_tank', 'water_tank')}),
        ('Brochure', {'fields': ('brochure',)}),
    )
    list_filter = ('shipyard', CabinFilter)
    inlines = [YachtPhotoGalleryInline]
    save_on_top = True
    form = select2_modelform(SailingYacht, attrs={'width': '250px'})
    formfield_overrides = {models.ImageField: {'widget': image_widgets.AdminImagePreviewWidget}}


class CatamaranAdmin(admin.ModelAdmin):
    form = select2_modelform(Catamaran, attrs={'width': '250px'})
    prepopulated_fields = {'slug': ('title', 'cabin')}
    fieldsets = (
        (None, {'fields': ('title', 'slug', 'built_year', 'shipyard', 'card_image')}),
        ('Accommodation', {'fields': ('cabin', 'bed', 'wc')}),
        ('Technical parameters', {'fields': ('length', 'beam', 'draft', 'engine_power', 'fuel_tank', 'water_tank')}),
        ('Equipment', {'fields': ('comfort_pack', 'entertainment')}),
        ('Brochure', {'fields': ('brochure',)}),
    )
    list_filter = ('shipyard', CabinFilter)
    inlines = [YachtPhotoGalleryInline]
    save_on_top = True
    formfield_overrides = {models.ImageField: {'widget': image_widgets.AdminImagePreviewWidget}}


class MotorYachtAdmin(admin.ModelAdmin):
    save_on_top = True
    prepopulated_fields = {'slug': ('title', 'cabin')}
    fieldsets = (
        (None, {'fields': ('title', 'slug', 'built_year', 'shipyard', 'card_image')}),
        ('Accommodation', {'fields': ('cabin', 'bed', 'wc')}),
        ('Technical parameters', {'fields': ('length', 'beam', 'draft', 'max_speed', 'cruising_speed', 'engine_power', 'fuel_consumption',
                                             'fuel_tank', 'water_tank')}),
        ('Brochure', {'fields': ('brochure',)}),
    )
    list_filter = ('shipyard', CabinFilter)
    inlines = [YachtPhotoGalleryInline]
    formfield_overrides = {models.ImageField: {'widget': image_widgets.AdminImagePreviewWidget}}


class GuletAdmin(admin.ModelAdmin):
    save_on_top = True
    prepopulated_fields = {'slug': ('title', 'cabin')}
    form = select2_modelform(Gulet, attrs={'width': '250px'})
    fieldsets = (
        (None, {'fields': ('title', 'slug', 'built_year', 'card_image', 'gulet_class', 'crew_num', 'crew_info')}),
        ('Accommodation', {'fields': ('cabin', 'cabin_info', 'bed', 'wc')}),
        ('Technical parameters', {'fields': ('length', 'beam')}),
        ('Equipment', {'fields': ('comfort_pack', 'entertainment')}),
        ('Brochure', {'fields': ('brochure',)}),
    )
    list_filter = (CabinFilter,)
    inlines = [YachtPhotoGalleryInline]
    formfield_overrides = {models.ImageField: {'widget': image_widgets.AdminImagePreviewWidget}}


class LuxYachtAdmin(admin.ModelAdmin):
    save_on_top = True
    prepopulated_fields = {'slug': ('title', 'cabin')}
    form = select2_modelform(LuxYacht, attrs={'width': '250px'})
    fieldsets = (
        (None, {'fields': ('title', 'slug', 'built_year', 'shipyard', 'card_image', 'crew_num', 'crew_info')}),
        ('Accommodation', {'fields': ('cabin', 'cabin_info', 'bed', 'wc')}),
        ('Technical parameters', {'fields': ('length', 'beam', 'max_speed', 'cruising_speed', 'engine_power',
                                             'fuel_consumption')}),
        ('Equipment', {'fields': ('comfort_pack', 'entertainment')}),
        ('Brochure', {'fields': ('brochure',)}),
    )
    list_filter = ('shipyard', CabinFilter)
    inlines = [YachtPhotoGalleryInline]
    formfield_overrides = {models.ImageField: {'widget': image_widgets.AdminImagePreviewWidget}}


class YachtCharterBareboatPriceLineInline(admin.TabularInline):
    model = YachtCharterBareboatPriceLine
    extra = 5


class YachtCharterBareboatPriceListAdmin(admin.ModelAdmin):
    save_on_top = True
    save_as = True
    form = select2_modelform(YachtCharterBareboatPriceList, attrs={'width': '250px'})
    list_filter = ('yacht_charter__country', 'yacht_charter__yacht__yacht_type')
    inlines = [YachtCharterBareboatPriceLineInline]
    list_display = ('__str__', 'order_id')
    list_editable = ('order_id',)

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "yacht_charter":
            kwargs["queryset"] = YachtCharter.objects.filter(yacht__yacht_type__yacht_type__in = (YachtType.SAILINGYACHT, YachtType.CATAMARAN, YachtType.MOTORYACHT))
        return super(YachtCharterBareboatPriceListAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)


class YachtCharterCrewedPriceLineInline(admin.TabularInline):
    model = YachtCharterCrewedPriceLine
    extra = 5


class YachtCharterCrewedPriceListAdmin(admin.ModelAdmin):
    save_on_top = True
    save_as = True
    form = select2_modelform(YachtCharterCrewedPriceList, attrs={'width': '250px'})
    list_filter = ('yacht_charter__country', 'yacht_charter__yacht__yacht_type')
    inlines = [YachtCharterCrewedPriceLineInline]
    list_display = ('__str__', 'order_id')
    list_editable = ('order_id',)

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "yacht_charter":
            kwargs["queryset"] = YachtCharter.objects.filter(yacht__yacht_type__yacht_type__in = (YachtType.GULET, YachtType.LUXYACHT))
        return super(YachtCharterCrewedPriceListAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)


class YachtEntertainmentAdmin(admin.ModelAdmin):
    list_display = ('title', 'order_id')
    list_editable = ('order_id',)


class YachtComfortPackAdmin(admin.ModelAdmin):
    list_display = ('title', 'order_id')
    list_editable = ('order_id',)


admin.site.register(Region, RegionAdmin)
admin.site.register(Country, CountryAdmin)
admin.site.register(CountryCharter, CountryCharterAdmin)
admin.site.register(ShipYard, ShipYardAdmin)
admin.site.register(YachtCharterBareboatPriceList, YachtCharterBareboatPriceListAdmin)
admin.site.register(YachtCharterCrewedPriceList, YachtCharterCrewedPriceListAdmin)
admin.site.register(YachtType, YachtTypeAdmin)
admin.site.register(YachtCharter, YachtCharterAdmin)
admin.site.register(SailingYacht, SailingYachtAdmin)
admin.site.register(Catamaran, CatamaranAdmin)
admin.site.register(MotorYacht, MotorYachtAdmin)
admin.site.register(Gulet, GuletAdmin)
admin.site.register(LuxYacht, LuxYachtAdmin)
admin.site.register(YachtEntertainment, YachtEntertainmentAdmin)
admin.site.register(YachtComfortPack, YachtComfortPackAdmin)
admin.site.register(GuletClass)

