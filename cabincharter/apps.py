from django.apps import AppConfig



class CabinCharterConfig(AppConfig):
    name = 'cabincharter'
    verbose_name = 'Cabin Charter'

    def ready(self):
        import cabincharter.signals