from django.contrib import admin
from django.utils import html

from easy_select2 import select2_modelform
from redactor.widgets import RedactorEditor
from photobank.admin import SinglePhotoInline
from utils import image_widgets

from .models import *



class RegionAdmin(admin.ModelAdmin):
    list_display = ('title', 'order_id')
    list_editable = ('order_id',)
    save_on_top = True


class PriceIncludeInline(admin.TabularInline):
    model = PriceInclude
    extra = 0


class PriceExcludeInline(admin.TabularInline):
    model = PriceExclude
    extra = 0


class OfferFilter(admin.SimpleListFilter):
    title = ('offer state')
    parameter_name = 'state'

    def lookups(self, request, model_admin):
        return (('current', 'Current'),
               ('archive', 'Archive'))

    def queryset(self, request, queryset):
        if self.value() == 'archive':
            return Offer.archive_objects
        elif self.value() == 'current':
            return Offer.current_objects


class OfferAdmin(admin.ModelAdmin):
    form = select2_modelform(Offer, attrs={'width': '250px'})
    formfield_overrides = {models.ImageField: {'widget': image_widgets.AdminImagePreviewWidget}}
    prepopulated_fields = {'slug': ('title',)}
    fieldsets = (
        (None, {'fields': ('region', 'title', 'slug', ('card_image', 'banner_image'), 'order_id')}),
        ('Offer card', {'classes': ('collapse',),
                        'fields': ('period', 'start_port', ('price', 'price_from'))}),
        ('Offer text info', {'classes': ('collapse',),
                             'fields': ('cruise_description', 'cruise_program', 'afterword',)}),
        ('Yacht', {'classes': ('collapse',),
                             'fields': ('yacht', 'cabin', 'wc',)}),
        ('Price description', {'classes': ('collapse',),
                               'fields': ('price_description',)}),
    )
    inlines = [PriceIncludeInline, PriceExcludeInline, SinglePhotoInline]
    list_display = ('title', 'order_id')
    list_editable = ('order_id',)
    save_on_top = True
    list_filter = [OfferFilter]

    def get_form(self, request, obj=None, **kwargs):
        if obj:
            self.formfield_overrides[models.TextField] = {'widget': RedactorEditor(redactor_options={'imageManagerJson': image_widgets.single_photo_json_url(obj.id, obj.photo_single.content_type.id),
                                                                                                     'buttonsHide': ['file']})}
        else:
            self.formfield_overrides[models.TextField] = {'widget': RedactorEditor()}
        return super(OfferAdmin, self).get_form(request, obj, **kwargs)


class CharterPriceLineInline(admin.TabularInline):
    model = CharterPriceLine
    extra = 5


class OfferPriceFilter(OfferFilter):
    def queryset(self, request, queryset):
        if self.value():
            if self.value() == 'archive':
                return queryset.filter(offer__in = Offer.archive_objects.values_list('id', flat=True))
            elif self.value() == 'current':
                return queryset.filter(offer__in = Offer.current_objects.values_list('id', flat=True))


class FilterTest(admin.SimpleListFilter):
    title = ('price')
    parameter_name = 'offer_id'

    def lookups(self, request, model_admin):
        offer_state = request.GET.get('state', None)
        if offer_state == 'archive':
            return [(offer.id, html.strip_tags(offer.title)[:35]) for offer in Offer.objects.filter(region__isnull = True).distinct()]
        elif offer_state == 'current':
            return [(offer.id, html.strip_tags(offer.title)[:35]) for offer in Offer.objects.filter(region__isnull = False).distinct()]
        else:
            return [(offer.id, html.strip_tags(offer.title)[:35]) for offer in Offer.objects.all()]

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(offer__id = self.value())


class CharterPriceAdmin(admin.ModelAdmin):
    inlines = [CharterPriceLineInline]
    form = select2_modelform(CharterPrice, attrs={'width': '250px'})
    save_on_top = True
    save_as = True
    list_filter = [OfferPriceFilter, FilterTest]
    list_display = ('__str__', 'order_id')
    list_editable = ('order_id',)



admin.site.register(Region, RegionAdmin)
admin.site.register(Offer, OfferAdmin)
admin.site.register(CharterPrice, CharterPriceAdmin)
