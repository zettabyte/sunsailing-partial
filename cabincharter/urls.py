from django.conf.urls import url

from .views import *



urlpatterns = [
    url(r'^$', Catalog.as_view(), name = 'main'),
    url(r'^arhiv/$', Archive.as_view(), name = 'offer_archive'),
    url(r'^(?P<slug>[\w-]+)/$', OfferDetail.as_view(), name = 'offer_detail'),
    ]