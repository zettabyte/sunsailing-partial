from django.views.generic import ListView, DetailView

from .models import Region, Offer



class Catalog(ListView):
    queryset = Region.objects.prefetch_related('offer_set', 'offer_set__yacht', 'offer_set__yacht__yacht_type').all()
    context_object_name = 'region_list'
    template_name = 'cabincharter/catalog.html'


class OfferDetail(DetailView):
    queryset = Offer.objects.prefetch_related('charterprice_set', 'charterprice_set__charterpriceline_set').all()
    template_name = "cabincharter/offer.html"


class Archive(ListView):
    queryset = Offer.archive_objects.prefetch_related('yacht', 'yacht__yacht_type').all()
    paginate_by = 24
    context_object_name = 'offer_list'
    template_name = 'cabincharter/archive.html'