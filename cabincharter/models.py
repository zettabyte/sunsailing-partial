import os

from django.db import models
from django.core.urlresolvers import reverse
from django.contrib.contenttypes.fields import GenericRelation

from utils import img_utils




class Region(models.Model):
    title = models.CharField(max_length=255)
    order_id = models.IntegerField( blank = True, null = True)
    
    def __str__(self):
        return self.title

    class Meta:
        ordering = ('order_id',)


class CurrentOfferManager(models.Manager):
    def get_queryset(self):
        return super(CurrentOfferManager, self).get_queryset().filter(region__isnull = False).distinct()


class ArchiveOfferManager(models.Manager):
    def get_queryset(self):
        return super(ArchiveOfferManager, self).get_queryset().filter(region__isnull = True).distinct()


class Offer(models.Model):
    region = models.ForeignKey(Region, null=True, blank=True)
    title = models.CharField(max_length=255)
    slug = models.SlugField(unique=True)
    banner_image = models.ImageField(upload_to='cabincharter/offer/banner_image/')
    card_image = models.ImageField(upload_to='cabincharter/offer/card_image/', help_text='Auto resize to 740px')

    period = models.CharField(max_length=255)
    start_port = models.CharField(max_length=255)
    price_from = models.BooleanField(default=False)
    price = models.PositiveIntegerField()

    cruise_description = models.TextField()
    cruise_program = models.TextField()
    afterword = models.TextField(blank=True, null=True)

    yacht = models.ForeignKey('yachtcharter.Yacht', null=True)
    cabin = models.CharField(max_length=255, null=True)
    wc = models.CharField(max_length=255, null=True)

    price_description = models.TextField(null=True, blank=True)

    order_id = models.PositiveIntegerField(blank=True, null=True)

    photo_single = GenericRelation('photobank.SinglePhoto')

    objects = models.Manager()
    current_objects = CurrentOfferManager()
    archive_objects = ArchiveOfferManager()

    def save(self, *args, **kwargs):
        if not os.path.isfile(self.card_image.path):
            self.card_image = img_utils.resize_ratio(740, self.card_image.file)
        super(Offer, self).save(*args, **kwargs)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('cabincharter:offer_detail', args=[self.slug])

    class Meta:
        ordering = ('order_id',)


class CharterPrice(models.Model):
    offer = models.ForeignKey(Offer)
    month_title = models.CharField(max_length=255)
    order_id = models.PositiveIntegerField(blank=True, null=True)

    class Meta:
        ordering = ('order_id',)

    def __str__(self):
        return self.month_title + ' | ' + self.offer.title


class CharterPriceLine(models.Model):
    charter_price = models.ForeignKey(CharterPrice)
    date_from = models.DateField()
    date_to = models.DateField()
    one_person_price = models.PositiveIntegerField()
    two_person_price = models.PositiveIntegerField(blank=True, null=True)
    three_person_price = models.PositiveIntegerField(blank=True, null=True)

    class Meta:
        ordering = ('date_from',)


class PriceInclude(models.Model):
    offer = models.ForeignKey(Offer)
    include = models.CharField(max_length=255)
    order_id = models.PositiveIntegerField(blank=True, null=True)

    class Meta:
        ordering = ('order_id',)


class PriceExclude(models.Model):
    offer = models.ForeignKey(Offer)
    exclude = models.CharField(max_length=255)
    order_id = models.PositiveIntegerField(blank=True, null=True)

    class Meta:
        ordering = ('order_id',)