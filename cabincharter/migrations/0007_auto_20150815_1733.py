# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cabincharter', '0006_auto_20150805_2101'),
    ]

    operations = [
        migrations.AlterField(
            model_name='offer',
            name='banner_image',
            field=models.ImageField(upload_to='cabincharter/offer/banner_image/'),
        ),
        migrations.AlterField(
            model_name='offer',
            name='card_image',
            field=models.ImageField(upload_to='cabincharter/offer/card_image/', help_text='Auto resize to 740px'),
        ),
    ]
