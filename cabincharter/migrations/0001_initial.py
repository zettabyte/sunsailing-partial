# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('yachtcharter', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='CharterPrice',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('month_title', models.CharField(max_length=255)),
                ('order_id', models.PositiveIntegerField(null=True, blank=True)),
            ],
            options={
                'ordering': ('order_id',),
            },
        ),
        migrations.CreateModel(
            name='CharterPriceLine',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('date_from', models.DateField()),
                ('date_to', models.DateField()),
                ('one_person_price', models.PositiveIntegerField()),
                ('two_person_price', models.PositiveIntegerField(null=True, blank=True)),
                ('three_person_price', models.PositiveIntegerField(null=True, blank=True)),
                ('charter_price', models.ForeignKey(to='cabincharter.CharterPrice')),
            ],
            options={
                'ordering': ('date_from',),
            },
        ),
        migrations.CreateModel(
            name='Offer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('slug', models.SlugField(unique=True)),
                ('image', models.ImageField(upload_to='cabincharter/offer/headers/')),
                ('thumbnail_image', models.ImageField(help_text='Auto resize to 330px', upload_to='cabincharter/offer/thumbnails/')),
                ('period', models.CharField(max_length=255)),
                ('start_port', models.CharField(max_length=255)),
                ('price_from', models.BooleanField(default=False)),
                ('price', models.PositiveIntegerField()),
                ('cruise_description', models.TextField()),
                ('cruise_program', models.TextField()),
                ('afterword', models.TextField()),
                ('cabin', models.CharField(null=True, max_length=255)),
                ('wc', models.CharField(null=True, max_length=255)),
                ('price_description', models.TextField(null=True, blank=True)),
                ('order_id', models.PositiveIntegerField(null=True, blank=True)),
            ],
            options={
                'ordering': ('order_id',),
            },
        ),
        migrations.CreateModel(
            name='PriceExclude',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('exclude', models.CharField(max_length=255)),
                ('offer', models.ForeignKey(to='cabincharter.Offer')),
            ],
        ),
        migrations.CreateModel(
            name='PriceInclude',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('include', models.CharField(max_length=255)),
                ('offer', models.ForeignKey(to='cabincharter.Offer')),
            ],
        ),
        migrations.CreateModel(
            name='Region',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('order_id', models.IntegerField(null=True, blank=True)),
            ],
            options={
                'ordering': ('order_id',),
            },
        ),
        migrations.AddField(
            model_name='offer',
            name='region',
            field=models.ForeignKey(to='cabincharter.Region', null=True, blank=True),
        ),
        migrations.AddField(
            model_name='offer',
            name='yacht',
            field=models.ForeignKey(to='yachtcharter.Yacht', null=True),
        ),
        migrations.AddField(
            model_name='charterprice',
            name='offer',
            field=models.ForeignKey(to='cabincharter.Offer'),
        ),
        migrations.CreateModel(
            name='Yacht',
            fields=[
            ],
            options={
                'proxy': True,
            },
            bases=('cabincharter.offer',),
        ),
    ]
