# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cabincharter', '0002_delete_yacht'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='priceexclude',
            options={'ordering': ('order_id',)},
        ),
        migrations.AlterModelOptions(
            name='priceinclude',
            options={'ordering': ('order_id',)},
        ),
        migrations.AddField(
            model_name='priceexclude',
            name='order_id',
            field=models.PositiveIntegerField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='priceinclude',
            name='order_id',
            field=models.PositiveIntegerField(blank=True, null=True),
        ),
    ]
