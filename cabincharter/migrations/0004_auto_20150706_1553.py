# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cabincharter', '0003_auto_20150704_2322'),
    ]

    operations = [
        migrations.AlterField(
            model_name='offer',
            name='thumbnail_image',
            field=models.ImageField(upload_to='cabincharter/offer/thumbnails/', help_text='Auto resize to 740px'),
        ),
    ]
