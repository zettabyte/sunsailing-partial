# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cabincharter', '0005_auto_20150710_1418'),
    ]

    operations = [
        migrations.AlterField(
            model_name='offer',
            name='afterword',
            field=models.TextField(null=True, blank=True),
        ),
    ]
